<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index');
///首页路由
Route::group(['namespace'=>'Media'], function () {
    Route::get('/', 'IndexController@index');
    Route::get('/article/{id}', 'ArticleController@index');
    Route::get('/feedback', 'IndexController@feedback');
    Route::get('/more', 'IndexController@more');
    Route::get('/search', 'IndexController@search');
    Route::get('/article/tag/{tag}', 'IndexController@articleByTag');
    Route::get('/cooperate', 'IndexController@cooperate');
});

//发布的路由
Route::group(['namespace'=>'Media','middleware'=>'auth'], function () {
    //发布
    Route::get('/publish_article', 'PublishController@article');
    Route::post('/article_create', 'PublishController@articleCreate');
    Route::post('/article_draft', 'PublishController@articleDraft');

    Route::get('/publish_video', 'PublishController@video');
    Route::get('/publish_pictures', 'PublishController@pictures');
    Route::get('/article_covers', 'PublishController@covers');
    Route::get('/article_cover_check', 'PublishController@coverCheck');
});

//文章
Route::group(['namespace'=>'Media','middleware'=>'auth'], function () {
    Route::get('/article_lists', 'ArticleController@articleLists');
    Route::get('/article_delete/{id}', 'ArticleController@articleDelete');
    Route::get('/article_edit/{id}', 'ArticleController@articleEdit');
    Route::get('/article_push/{id}', 'ArticleController@articlePush');
});
//图片上传
Route::group(['namespace'=>'Media','middleware'=>'auth'], function () {
    Route::post('/upload_picture', 'UploadController@picture');
    Route::post('/upload_pictures', 'UploadController@pictures');
    Route::post('/upload_video', 'UploadController@video');
    Route::post('/upload_avatar', 'UploadController@avatar');
});

//账户
Route::group(['namespace'=>'Media','middleware'=>'auth'], function () {
    Route::get('/account', 'AccountController@index');
    Route::get('/account_avatar', 'AccountController@avatar');
    Route::get('/account_info', 'AccountController@info');
    Route::get('/account_api', 'AccountController@api');
});

//评论
Route::group(['namespace'=>'Media','middleware'=>'auth'], function () {
    Route::get('/comments_manager', 'CommentController@commentManager');
    Route::post('/comment_create', 'CommentController@commentCreate');
    Route::post('/comment_delete/{id}', 'CommentController@commentDelete');
    Route::get('/get_reply/comment_uid/{comment_uid}/reply_to/{author}', 'CommentController@getReply');
});



//头条接口
Route::group(['namespace'=>'Media'], function () {
    Route::get('/toutiao_callback', 'ToutiaoController@callback');
    Route::post('/toutiao_authorize', 'ToutiaoController@getAuthorize');
    Route::post('/toutiao_create_api', 'ToutiaoController@createApi');
});















//admin
Route::group(['namespace'=>'Admin','prefix'=>'jzopenos'], function () {
    Route::match(['post','get'], '/login', 'AuthController@login');
    Route::get('/logout', 'AuthController@logout');
});

Route::group(['namespace'=>'Admin','prefix'=>'jzopenos','middleware'=>'jzauth'], function () {
    Route::get('/index', 'AdminController@index');
});
//Carousel
Route::group(['namespace'=>'Admin','prefix'=>'jzopenos/carousel','middleware'=>'jzauth'], function () {
    Route::get('/list', 'CarouselController@list');
    Route::post('/add', 'CarouselController@add');
    Route::post('/delete', 'CarouselController@delete');
    Route::match(['post','get'], '/edit', 'CarouselController@edit');
    Route::match(['post','get'], '/create', 'CarouselController@create');
});
//Article
Route::group(['namespace'=>'Admin','prefix'=>'jzopenos/article','middleware'=>'jzauth'], function () {
    Route::get('/list', 'ArticleController@list');
});
