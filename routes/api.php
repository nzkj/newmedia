<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->get('/get_article_comments', 'App\Http\Controllers\Api\v1\CommentController@getArticleComments');
    $api->get('/get_comment_replys', 'App\Http\Controllers\Api\v1\CommentController@getCommentReplys');
    $api->post('/post_comment', 'App\Http\Controllers\Api\v1\CommentController@postComment');
    $api->get('/get_more_comments', 'App\Http\Controllers\Api\v1\CommentController@getMoreComments');
});
