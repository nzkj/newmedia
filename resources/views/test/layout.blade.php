<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="/plugins/semantic/semantic.css" rel="stylesheet">
    <script src="/test/vue.js" charset="utf-8"></script>
    <script src="/test/axios.js" charset="utf-8"></script>
</head>

<body>
<div class="pusher">
    @section('main')

    @show
    
    @section('js')

    @show
</div>
</body>

</html>
