
<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

    <meta charset="utf-8">
    <title>轻说|Lightsays</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/media/reset.css">
    <link rel="stylesheet" href="/css/media/supersized.css">
    <link rel="stylesheet" href="/css/media/style.css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body oncontextmenu="return false">

<div class="page-container">
    <h1>登录</h1>
    <form  method="POST" action="{{ url('/login') }}">
        {{ csrf_field()  }}
        <div>
            <input type="text" name="email" class="username" placeholder="邮箱" >
        </div>
        <div>
            <input type="password" name="password" class="password" placeholder="密码">
        </div>
        <button id="submit" type="submit">登录</button>
    </form>
    <div class="connect">
        <p>Sent your articles to the world</p>
        <p style="margin-top:20px;">让世界听到你的声音</p>
    </div>
</div>
<div class="alert" style="display:none">
    <h2>消息</h2>
    <div class="alert_con">
        <p id="ts"></p>
        <p style="line-height:70px"><a class="btn">确定</a></p>
    </div>
</div>

<!-- Javascript -->
<script src="//cdn.bootcss.com/jquery/1.12.4/jquery.js"></script>
<script src="/js/media/supersized.3.2.7.min.js"></script>
<script src="/js/media/supersized-init.js"></script>
</body>

</html>
