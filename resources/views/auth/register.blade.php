
<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

    <meta charset="utf-8">
    <title>轻说|Lightsays</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/media/reset.css">
    <link rel="stylesheet" href="/css/media/supersized.css">
    <link rel="stylesheet" href="/css/media/style.css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body oncontextmenu="return false">

<div class="page-container">
    <h1>注册</h1>
    <form role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field()  }}
        <div>
            <input type="text" name="name" class="username" placeholder="用户名" autocomplete="off"/>
        </div>
        <div>
            <input type="text" name="email" class="username" placeholder="邮箱" autocomplete="off"/>
        </div>
        <div>
            <input type="password" name="password" class="password" placeholder="密码" />
        </div>
        <div>
            <input type="password" name="password_confirmation" class="password" placeholder="确认密码" />
        </div>
        <button id="submit" type="submit">注册</button>
    </form>
</div>

<!-- Javascript -->
<script src="//cdn.bootcss.com/jquery/1.12.4/jquery.js"></script>
<script src="/js/media/supersized.3.2.7.min.js"></script>
<script src="/js/media/supersized-init.js"></script>
</body>

</html>
