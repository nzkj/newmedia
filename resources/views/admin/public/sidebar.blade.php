@section('sidebar')
<div class="ui vertical visible sidebar menu" id="sidebar">
    <div class="item" id="open_sidebar" style="min-height: 120px;background-color: #243544">
        <img src="/images/jenny.jpg" alt="" id="avatar" class="ui tiny avatar image" style="display: block; margin: 15px">
        <div class="ui dropdown" id="admin" style="display: block;margin: 0 0 40px 20px;color: white">
            {{ Session::get('adminInfo')->display_name }}
            <i class="icon dropdown"></i>
            <div class="menu">
                <a href="#" class="item"><i class="icon green edit"></i>完善资料</a>
                <a href="#" class="item"><i class="icon pink photo"></i>修改头像</a>
                <a href="#" class="item"><i class="icon yellow lock"></i>账户安全</a>
                <a href="#" class="item"><i class="icon red sign out"></i>退出登录</a>
            </div>
        </div>
    </div>
    <div class="item" id="mimi_sidebar" style="display: none">
        <img src="/images/jenny.jpg" alt=""  class="ui avatar image">
    </div>
    <div class="ui hidden divider"></div>
    <!--menu-->
    <a href="/jzopenos/dashboard" class="item {{ Request::path()=='jzopenos' ? 'active' : '' }}">
        <i class="icon dashboard"></i>
        <span class="sidebar_span">控制台</span>
        <i class="icon angle left" style=" float: right ;margin-right: 0"></i>
    </a>
    <a href="/jzopenos/article/list" class="item {{ Request::path()=='jjzopenos/article/list' ? 'active' : '' }}">
        <i class="icon translate"></i>
        <span class="sidebar_span">文章管理</span>
        <i class="icon angle left" style=" float: right ;margin-right: 0"></i>
    </a>
    <a href="/jzopenos/reviewed/list" class="item {{ Request::path()=='jzopenos/reviewed/list' ? 'active' : '' }}">
        <i class="icon adjust"></i>
        <span class="sidebar_span">审核管理</span>
        <i class="icon angle left" style=" float: right ;margin-right: 0"></i>
    </a>
    <a href="/jzopenos/comment/list" class="item {{ Request::path()=='jzopenos/comment/list' ? 'active' : '' }}">
        <i class="icon comments"></i>
        <span class="sidebar_span">评论管理</span>
        <i class="icon angle left" style=" float: right ;margin-right: 0"></i>
    </a>
    <a href="/jzopenos/adv/list" class="item {{ Request::path()=='jzopenos/adv/list' ? 'active' : '' }}">
        <i class="icon thumbs outline up"></i>
        <span class="sidebar_span">推荐管理</span>
        <i class="icon angle left" style=" float: right ;margin-right: 0"></i>
    </a>
    <a href="/jzopenos/carousel/list" class="item {{ Request::path()=='/jzopenos/carousel/list' ? 'active' : '' }}">
        <i class="icon fast forward"></i>
        <span class="sidebar_span">轮播管理</span>
        <i class="icon angle left" style=" float: right ;margin-right: 0"></i>
    </a>


</div>
@show
