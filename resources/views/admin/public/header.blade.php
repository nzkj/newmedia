@section('header')
<div class="ui secondary pointing menu">
        <a class="item toggle-sidebar"><i class="icon list"></i></a>
        <div class="right menu">
            <form action="/search" class="qj_search_form" style="display: none">
                <div class="field">
                    <div class="ui input">
                        <input type="search taggle-search" placeholder="Search.." style="width: 600px;border: 0">
                    </div>
                </div>
            </form>
            <a href="/search" class="item"><i class="icon red search"></i></a>
            <a href="/notify" class="item"><i class="icon yellow bell"></i></a>
            <a href="/" class="item"><i class="icon green home"></i></a>
            <a href="/jzopenos/logout" class="item"><i class="icon sign red out "></i></a>
        </div>
    </div>
@show