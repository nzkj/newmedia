@section('js')
   <script>
    $('.ui.dropdown')
            .dropdown()
    ;
    //侧边栏
    $(".toggle-sidebar").click(function () {
        if ($("#sidebar").hasClass('icon')){
            $("#sidebar").removeClass('icon');
            $("span.sidebar_span").css('display','inline-block');
            $("i.icon.angle.left").css('display','inline-block')
            $("div.pusher").css('margin-left','210px');
            $("#mimi_sidebar").css('display','none')
            $("#open_sidebar").css('display','block')

        }else{
            $("#sidebar").addClass('icon');
            $("span.sidebar_span").css('display','none');
            $("i.icon.angle.left").css('display','none')
            $("div.pusher").css('margin-left','60px');
            $("#mimi_sidebar").css('display','block')
            $("#open_sidebar").css('display','none')

        }
    });
    //搜索
    $(".red.search").click(function () {
        $("form.qj_search_form").toggle().transition('left');
    })
    //modal
    $("button.add_gzh").click(function () {
        $('.ui.modal')
                .modal('show')
        ;
    })

</script>
@show