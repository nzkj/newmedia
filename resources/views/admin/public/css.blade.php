@section('css')
   <meta charset="UTF-8">
    <title>admin</title>
    <link rel="stylesheet" href="/plugins/semantic/semantic.css">
    <link rel="stylesheet" href="/plugins/layer/skin/default/layer.css">
    <script src="/plugins/jquery/jquery-3.1.1.min.js"></script>
    <script src="/plugins/semantic/semantic.js"></script>
    <script src="/plugins/layer/layer.js"></script>
    <style>
        .ui.sidebar.menu{
            background-color: #2F4050;
        }
        .ui.sidebar.menu>a,.ui.sidebar.menu.icon>a{
            color: #A7B1C2;
        }
        .ui.sidebar.menu>a:hover{
              color: #fff2f2;
        }
        .ui.sidebar.menu>a.active{
            color: #fff2f2;
        }

        .ui.vertical.menu .item > i.icon{
            float: none;
            margin-right: .5em;
        }

        .ui.menu.icon{
            min-width: 50px;
        }
        div.pusher{
            margin-left: 210px;
        }
        #open_sidebar{
            background: url("/images/header-profile.png") no-repeat;;
        }
    </style>
@show
