@extends('admin.layout')
@section('main')
<div class="ui red segment" style="margin: 10px">
	<div class="ui two column grid">
		<div class="column">
			<div class="ui segment">
				<h3><i class="icon brown dollar"></i>签到积分规则</h3>
				<form action="" method="get" accept-charset="utf-8" class="ui form">
					<div class="inline field">
						<label for="">签到1天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">
					</div>
					<div class="inline field">
						<label for="">签到2天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">

					</div>
					<div class="inline field">
						<label for="">签到3天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">

					</div>
					<div class="inline field">
						<label for="">签到4天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">

					</div>
					<div class="inline field">
						<label for="">签到5天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">

					</div>
					<div class="inline field">
						<label for="">签到6天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">

					</div>
					<div class="inline field">
						<label for="">签到7天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">

					</div>
					<div class="inline field">
						<label for="">大于7天</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="签到描述">

					</div>
				</form>
			</div>
		</div>
		<div class="column">
			<div class="ui segment">
				<h3><i class="icon brown dollar"></i>套餐积分规则</h3>
				<form action="" method="get" accept-charset="utf-8" class="ui form">
					<div class="inline field">
						<label for="">挂机套餐</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="">
					</div>
					<div class="inline field">
						<label for="">VIP套餐</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="">
					</div>
					<div class="inline field">
						<label for="">VIP套餐</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="">
					</div>
					<div class="inline field">
						<label for="">VIP套餐</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="">
					</div>
					<div class="inline field">
						<label for="">VIP套餐</label>
						<input type="text" name="" value="" placeholder="">
						<input type="text" name="" value="" placeholder="">
					</div>
				</form>
			</div>
		</div>
		<div class="column">
			<div class="ui segment">
				<h3><i class="icon brown dollar"></i>积分兑换规则</h3>
				<form action="" method="get" accept-charset="utf-8" class="ui form">
					<div class="inline field">
						<label for="">签到</label>
						<input type="text" name="" value="" placeholder="">
					</div>
				</form>
			</div>
		</div>
		<div class="column">
			<div class="ui segment">
				<h3><i class="icon brown dollar"></i>积分邀请规则</h3>
				<form action="" method="get" accept-charset="utf-8" class="ui form">
					<div class="inline field">
						<label for="">签到</label>
						<input type="text" name="" value="" placeholder="">
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@stop
@section('js')
@parent

@stop
