@extends('admin.layout')
@section('main')
<div class="ui red segment" style="margin: 10px">
	<h3>轮播
		<span style="float:right">
			<button class="ui tiny button primary is_ajax_get"><i class="icon plus"></i>添加</button>
		</span>
	</h3>
	<table class="ui table">
	    <thead>
	          <tr>
	              <th>编号</th>
	              <th>名称</th>
	              <th>封面</th>
	              <th>链接</th>
	              <th>创建时间</th>
	              <th>控制台</th>
	          </tr>
	    </thead>
	    <tbody>
	            @foreach ($lists as $list)
	                <tr>
	                  	<td>{{ $list->id }}</td>
	                  	<td>{{ $list->display_name }}</td>
	                  	<td><img src="{{$list->cover}}" alt="" class="ui tiny image"></td>
	                  	<td>{{ $list->url }}</td>
	                  	<td>{{ $timeAgo->inWords($list->created_at) }}</td>
		                <td><button class="ui small icon button is_ajax_post" data-id="{{ $list->id }}"><i class="icon red trash"></i></button>
		                <button class="ui small icon button is_ajax_get" data-id="{{ $list->id }}"><i class="icon green info"></i></button></td>
	                </tr>
	            @endforeach
	    </tbody>
	</table>
	 <div class="ui container grid">
        <div class="centered column">
            {{ $lists->links('vendor.pagination.semantic') }}
        </div>
    </div>
</div>
<div class="ui modal">
    <i class="close icon"></i>
    <div class="header">
    </div>
    <div class="content">
		<form class="ui form" action="/carousel/create" method="post">
			<div class="field">
				<label for="">标题</label>
				<input type="text" name="display_name" value="">
			</div>
			<div class="field">
				<label for="">摘要</label>
				<textarea name="abstract" rows="5" cols="80"></textarea>
			</div>
			<div class="fields">
				<div class="field">
					<label for="">文章ID</label>
					<input type="text" name="article_id" value="">
				</div>
				<div class="field">
					<label for="">文章链接</label>
					<input type="text" name="url" value="">
				</div>
				<div class="field">
					<label for="">文章排序</label>
					<input type="text" name="sort" value="">
				</div>
				<div class="field">
					<label for="">立刻生效</label>
					<div class="ui radio checkbox">
	  					<input type="radio" name="status" checked="checked">
	  					<label>是</label>
					</div>
					<div class="ui radio checkbox">
	  					<input type="radio" name="status">
	  					<label>否</label>
					</div>
				</div>
			</div>
			<div class="field">
				<label for="">上传封面</label>
				<div class="ui segment" style="width:80px;heigh:80px;padding:30px">
					<i class="icon plus"></i>
				</div>
				<input type="hidden" name="cover" value="">
				<input type="hidden" name="thumb_cover" value="">
			</div>
			<button type="button" class="ui primary button" name="button">提交</button>
		</form>
    </div>
</div>
@stop
@section('js')
@parent
<script>
	$(".is_ajax_post").click(function () {
		var url = "{{ url('jzopenos/carousel/delete') }}";
		var id = $(this).data('id');
		var data ={
				"_token":"{{ csrf_token() }}",
				'id':id
			}
		layer.confirm('您确定删除该用户？删除后数据不可恢复！', {
		  btn: ['确定','取消'] //按钮
		}, function(){
			$.post(url,data, function(result) {
				result = $.parseJSON(result);
				if (result.status ==1) {
	              layer.msg(result.info, {
	                        offset: 20,
	                        shift: 5
	                    }, function() {
	                        if (result.url) {
	                            window.location.href = result.url;
	                        }
	                    });

	            }else if (result.status ==0) {
	              layer.msg(result.info, {
	                        offset: 20,
	                        shift: 5
	                    }, function() {
	                        if (result.url) {
	                            window.location.href = result.url;
	                        }
	                    });
	            }
			});
		}, function(){
			var delete_it = 0
		  	layer.msg('您已取消删除！', {
                offset: 20,
                shift: 5
            });
		});
	});
	$(".is_ajax_get").click(function() {
        $('.ui.modal')
                .modal('show',{
                    transition: 'fade up'
                })
        ;
	});
</script>
@stop
