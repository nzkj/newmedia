@extends('admin.layout')
@section('main')
<div class="ui red segment" style="margin: 10px">
	<table class="ui table">
	    <thead>
	          <tr>
	              <th>编号</th>
	              <th>名称</th>
	              <th>封面</th>
	              <th>创建时间</th>
	              <th>控制</th>
	          </tr>
	    </thead>
	    <tbody>
	            @foreach ($lists as $list)
	                <tr>
	                  	<td>{{ $list->id }}</td>
	                  	<td><a href="/article/{{$list->id}}">{{ $list->title }}</a></td>
	                  	<td><img src="{{ $list->cover1 }}" alt="" class="ui tiny image"></td>
	                  	<td>{{ $timeAgo->inWords($list->created_at) }}</td>
		                <td><button class="ui small icon button is_ajax_post" data-id="{{ $list->id }}" data-title="{{ $list->title }}"  data-cover="{{ $list->cover1 }}"><i class="icon red rss"></i></button>
		                <button class="ui small icon button is_ajax_get" data-id="{{ $list->id }}"><i class="icon green info"></i></button></td>
	                </tr>
	            @endforeach
	    </tbody>
	</table>
	 <div class="ui container grid">
        <div class="centered column">
            {{ $lists->links('vendor.pagination.semantic') }}
        </div>
    </div>
</div>
<div class="ui modal">
    <i class="close icon"></i>
    <div class="header">
    </div>
    <div class="content">
		<div class="ui grid">
			<div class="six wide column">
				<div class="ui card">
				  <div class="content">
				    <div class="right floated meta" id="wxcard_vip_last"></div>
				    <img class="ui avatar image" id="wxcard_head" src=""><span id="wxcard_nickname"></span> id:<span id="wxcard_id"></span>uid:<span id="wxcard_uid"></span>
				  </div>
				  <div class="image">
				  	<a class="ui red left corner label" style="display: block;">
	        		<i class="heart icon"></i>
	      			</a>
				  	<img class="ui fluid image" id="wxcard_code" src="">
				  </div>
				  <div class="content">
				    <span class="right floated" id="wxcard_hot">
				    </span>
				    <span id="wxcard_city"></span>
				  </div>
				</div>
			</div>
			<div class="five wide column">
				<form class="ui form" action="" method="get" accept-charset="utf-8">
					<div class="ui field">
						<label for="">修改好友</label>
						<div class="ui action input">
							<input type="text" name="hot" value="" placeholder="">
						 	<button class="ui teal button">修改</button>
						</div>
					</div>
					<div class="ui field">
						<label for="">赠送积分</label>
						<div class="ui action input">
							<input type="text" name="score" value="" placeholder="" id="score_by_manager">
						 	<button class="ui teal button score_by_manager_click" type="button">赠送</button>
						</div>
					</div>
					<div class="ui field">
						<label for="">设置暴机</label>
						<div class="ui action input">
							<input type="text" name="hot" value="" placeholder="">
						 	<button class="ui teal button">时长</button>
						</div>
					</div>
					<div class="ui field">
						<label for="">人工置顶</label>
						<div class="ui action input">
							<input type="text" name="hot" value="" placeholder="">
						 	<button class="ui teal button">置顶</button>
						</div>
					</div>
				</form>
			</div>
			<div class="five wide column">
				<form class="ui form" action="">
					<div class="field">
					    <div class="ui toggle checkbox">
					      	<label>设置暴机</label>
					      	<input type="checkbox" tabindex="0" class="hidden">
					    </div>
					</div>
					<div class="field">
				    	<div class="ui toggle checkbox">
				    		<label>黑名单</label>
				      		<input type="checkbox" tabindex="0" class="hidden">
				    	</div>
					</div>
					<div class="field">
				    	<div class="ui toggle checkbox">
				    		<label>黑名单</label>
				      		<input type="checkbox" tabindex="0" class="hidden">
				    	</div>
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
@stop
@section('js')
@parent
<script>
	$(".is_ajax_post").click(function () {
		var url = "{{ url('jzopenos/carousel/add') }}";
		var id = $(this).data('id');
		var title = $(this).data('title');
		var cover = $(this).data('cover');
		var data ={
				"_token":"{{ csrf_token() }}",
				'id':id,
				'title':title,
				'cover':cover
			}
		layer.confirm('是否要确定要设置首页轮播？', {
		  btn: ['确定','取消'] //按钮
		}, function(){
			$.post(url,data, function(result) {
				result = $.parseJSON(result);
				if (result.status ==1) {
	              layer.msg(result.info, {
	                        offset: 20,
	                        shift: 5
	                    }, function() {
	                        if (result.url) {
	                            window.location.href = result.url;
	                        }
	                    });

	            }else if (result.status ==0) {
	              layer.msg(result.info, {
	                        offset: 20,
	                        shift: 5
	                    }, function() {
	                        if (result.url) {
	                            window.location.href = result.url;
	                        }
	                    });
	            }
			});
		}, function(){
			var delete_it = 0
		  	layer.msg('您已取消删除！', {
                offset: 20,
                shift: 5
            });
		});
	});
	$(".is_ajax_get").click(function() {
        $('.ui.modal')
                .modal('show',{
                    transition: 'fade up'
                })
        ;
	});
</script>
@stop
