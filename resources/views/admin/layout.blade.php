<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.public.css')
</head>
<body>
@include('admin.public.sidebar')
<div class="pusher">
    @include('admin.public.header')
   	@section("main")

   	@show
</div>
    @include('admin.public.js')
</body>
</html>
