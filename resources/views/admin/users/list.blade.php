@extends('admin.layout')
@section('main')
<div class="ui red segment" style="margin: 10px">
	<div class="ui horizontal brown statistic">
	  	<div class="value">
	   	{{ $count }}个
	  	</div>
	 	<div class="label">
	    用户总数
	  	</div>
	</div>
	<div class="ui horizontal brown statistic">
	  	<div class="value">
	   	{{ $bind_phone }}
	  	</div>
	 	<div class="label">
	    手机绑定
	  	</div>
	</div>
	<div class="ui horizontal brown statistic">
	  	<div class="value">
	   	{{ $bind_email }}
	  	</div>
	 	<div class="label">
	    邮箱绑定
	  	</div>
	</div>
	<div class="ui horizontal red statistic">
		<div class="value">
		    {{ $bind_wx }}
		</div>
		<div class="label">
		    微信绑定
		</div>
	</div>
	<div class="ui horizontal red statistic">
		<div class="value">
		    {{ $today_add }}
		</div>
		<div class="label">
		    新增用户
		</div>
	</div>
	<table class="ui table">
	    <thead>
	          <tr>
	              <th>编号</th>
	              <th>昵称</th>
	              <th>头像</th>
	              <th>邮箱</th>
	              <th>手机</th>
	              <th>创建时间</th>
	          </tr>
	    </thead>
	    <tbody>
	            @foreach ($users as $user)
	                <tr>
	                  	<td>{{ $user->id }}</td>
	                  	<td>{{ $user->name }}</td>
	                  	<td><img src="{{ $user->avatar }}" alt="" class="ui avatar image"></td>
	                  	<td>{{ $user->email }}</td>
	                  	<td>{{ $user->phone }}</td>
	                  	<td>{{ $user->created_at }}</td>
		                <td><button class="ui small icon button is_ajax_post" data-id="{{ $user->id }}"><i class="icon red trash"></i></button>
		                <button class="ui small icon button is_ajax_get" data-id="{{ $user->id }}"><i class="icon green info"></i></button></td>
	                </tr>
	            @endforeach
	    </tbody>
	</table>
	 <div class="ui container grid">
        <div class="centered column">
            {{ $users->links('vendor.pagination.semantic') }}
        </div>
    </div>
</div>
<div class="ui modal">
    <i class="close icon"></i>
    <div class="header">
    </div>
    <div class="content">
		<div class="ui grid">
			<div class="six wide column">
				<div class="ui card">
				  <div class="content">
				    <div class="right floated meta" id="wxcard_vip_last"></div>
				    <img class="ui avatar image" id="wxcard_head" src=""><span id="wxcard_nickname"></span> id:<span id="wxcard_id"></span>uid:<span id="wxcard_uid"></span>
				  </div>
				  <div class="image">
				  	<a class="ui red left corner label" style="display: block;">
	        		<i class="heart icon"></i>
	      			</a>
				  	<img class="ui fluid image" id="wxcard_code" src="">
				  </div>
				  <div class="content">
				    <span class="right floated" id="wxcard_hot">
				    </span>
				    <span id="wxcard_city"></span>
				  </div>
				</div>
			</div>
			<div class="five wide column">
				<form class="ui form" action="" method="get" accept-charset="utf-8">
					<div class="ui field">
						<label for="">修改好友</label>
						<div class="ui action input">
							<input type="text" name="hot" value="" placeholder="">
						 	<button class="ui teal button">修改</button>
						</div>
					</div>
					<div class="ui field">
						<label for="">赠送积分</label>
						<div class="ui action input">
							<input type="text" name="score" value="" placeholder="" id="score_by_manager">
						 	<button class="ui teal button score_by_manager_click" type="button">赠送</button>
						</div>
					</div>
					<div class="ui field">
						<label for="">设置暴机</label>
						<div class="ui action input">
							<input type="text" name="hot" value="" placeholder="">
						 	<button class="ui teal button">时长</button>
						</div>
					</div>
					<div class="ui field">
						<label for="">人工置顶</label>
						<div class="ui action input">
							<input type="text" name="hot" value="" placeholder="">
						 	<button class="ui teal button">置顶</button>
						</div>
					</div>
				</form>
			</div>
			<div class="five wide column">
				<form class="ui form" action="">
					<div class="field">
					    <div class="ui toggle checkbox">
					      	<label>设置暴机</label>
					      	<input type="checkbox" tabindex="0" class="hidden">
					    </div>
					</div>
					<div class="field">
				    	<div class="ui toggle checkbox">
				    		<label>黑名单</label>
				      		<input type="checkbox" tabindex="0" class="hidden">
				    	</div>
					</div>
					<div class="field">
				    	<div class="ui toggle checkbox">
				    		<label>黑名单</label>
				      		<input type="checkbox" tabindex="0" class="hidden">
				    	</div>
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
@stop
@section('js')
@parent
<script>
	$(".is_ajax_post").click(function () {
		var url = "{{ url('jzopenos/s_gzh/system/users/delete') }}";
		var id = $(this).data('id');
		var data ={
				"_token":"{{ csrf_token() }}",
				'id':id
			}
		layer.confirm('您确定删除该用户？删除后数据不可恢复！', {
		  btn: ['确定','取消'] //按钮
		}, function(){
			$.post(url,data, function(result) {
				result = $.parseJSON(result);
				if (result.status ==1) {
	              layer.msg(result.info, {
	                        offset: 20,
	                        shift: 5
	                    }, function() {
	                        if (result.url) {
	                            window.location.href = result.url;
	                        }
	                    });

	            }else if (result.status ==0) {
	              layer.msg(result.info, {
	                        offset: 20,
	                        shift: 5
	                    }, function() {
	                        if (result.url) {
	                            window.location.href = result.url;
	                        }
	                    });
	            }
			});
		}, function(){
			var delete_it = 0
		  	layer.msg('您已取消删除！', {
                offset: 20,
                shift: 5
            });
		});
	});
	$(".is_ajax_get").click(function() {

		$('.ui.modal')
                .modal('show',{
                    transition: 'fade up'
                })
        ;
	});
</script>
@stop
