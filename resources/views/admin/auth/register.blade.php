<!doctype html>
<html lang="zh">
<head>
    @include('admin.public.css')
</head>
<body>
<div class="ui container">
    <div class="ui grid">

            <div class="row">
                <div class="three wide column"></div>
                <div class="ten wide column">
                    <h1 style="text-align: center;margin-top: 80px;">Wxsys</h1>
                </div>
            </div>
        <div class="row">

            <div class="column" style="padding: 0 35%">
                <div class="ui column middle aligned relaxed grid">
                    <div class="column">
                        <form action="" method="post">
                        <div class="ui form">
                            <div class="field">
                                <label>称呼</label>
                                <div class="ui input">
                                    <input type="text" name="nickname" placeholder="输入公司名称">
                                </div>
                            </div>
                            <div class="field">
                                <label>邮箱</label>
                                <div class="ui input">
                                    <input type="text" name="email" placeholder="Email">
                                </div>
                            </div>
                            {!! csrf_field() !!}
                        </form>
                        <div class="ui red segment">
                            <span><small>仅支持邮箱注册!请使用企业邮箱注册.</small></span>
                        </div>
                            <a class="ui blue fluid button is_ajax_post">下一步</a>

                            <p style="text-align: right">&copy; CopyRight wxsys.sznzkj.cn 2015-2016</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.is_ajax_post').click(function () {
    //获取父级元素的同级 是form
    var form = $("form");
    var url = form.attr("action");
    var data = form.serialize();
    $.post(url,data,function (result) {
        result = eval('('+result+')');
        if (result.status = 1) {
            layer.msg(result.info,
                {
                    offset:20,
                    shift:5
                },function(){
                    if (result.url) {
                    window.location.href = result.url;
                    }
                });
        }else if (result.status = 0) {
            layer.msg(result.info,
                {
                    offset:50,
                    shift:5
                },function(){
                    if (result.url) {
                        window.location.href = result.url;
                    }
                });
        }
    });
});
</script>
</body>
</html>