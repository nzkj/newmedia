<!doctype html>
<html lang="zh">
<head>
    @include('admin.public.css')
</head>
<body>
<div class="ui container">
    <div class="ui grid">

            <div class="row">
                <div class="three wide column"></div>
                <div class="ten wide column">
                    <h1 style="text-align: center;margin-top: 80px;">完善基本信息</h1>
                </div>
            </div>
        <div class="row">

            <div class="column" style="padding: 0 35%">
                <div class="ui column middle aligned relaxed grid">
                    <div class="column">
                        <form action="" method="post">
                        <div class="ui form">
                            <div class="field">
                                <label>手机号</label>
                                <div class="ui input">
                                    <input type="text" name="username" placeholder="手机号为登录名">
                                </div>
                            </div>
                            <div class="field">
                                <label>密码</label>
                                <div class="ui input">
                                    <input type="text" name="password" placeholder="至少六位">
                                </div>
                            </div>
                            {!! csrf_field() !!}
                        </form>
                        <div class="ui red segment">
                            <span><small>手机号为登录名,必填</small></span>
                        </div>
                            <a class="ui blue fluid button is_ajax_post">完成并激活</a>

                            <p style="text-align: right">&copy; CopyRight wxsys.sznzkj.cn 2015-2016</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.is_ajax_post').click(function () {
    //获取父级元素的同级 是form
    var form = $("form");
    var url = form.attr("action");
    var data = form.serialize();
    $.post(url,data,function (result) {
        result = eval('('+result+')');
        if (result.status = 1) {
            layer.msg(result.info,
                {
                    offset:20,
                    shift:5
                },function(){
                    if (result.url) {
                    window.location.href = result.url;
                    }
                });
        }else if (result.status = 0) {
            layer.msg(result.info,
                {
                    offset:50,
                    shift:5
                },function(){
                    if (result.url) {
                        window.location.href = result.url;
                    }
                });
        }
    });
});
</script>
</body>
</html>