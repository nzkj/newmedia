@extends('media.layout.layout')
@section('css')
    @parent
    <style>
        .page-header{
            padding-bottom: 0.7em;
            position: relative;
            border-bottom: 1px solid #eee;
        }
        .page-header:after{
            position: absolute;
            content: "";
            width: 1em;
            height: 0.2em;
            left: 0;
            bottom: -1px;
            background-color: #f7505a;
            -webkit-transition:width 1s;
            transition: width 0.2s;
        }
        .page-header:hover:after{
            width: 5em;
        }
        div.nav-item{
            margin:.5em 0;
            height: 40px;
            text-align: center;;
            line-height: 40px;
            font-size: 16px;
        }
        div.nav-item>a{
            color: #333;
        }
        div.nav-item.active,div.nav-item:hover{
            border:1px;
            border-radius:5px;
            background:#FF635C;
            height: 40px;
            text-align: center;;
            color: white;
            line-height: 40px;
            font-size: 16px;
        }
        div.nav-item.active>a,div.nav-item:hover>a{
            color: white;

        }
    </style>
@endsection

@section('main')
    <div class="ui container grid">
        <div class="two wide column computer only navbar_wrapper_pin" style="padding:1em .5em;">
            <div class="sidebar_pin">
                <h1 style="color:#FF635C;text-align:center;"><a href="/" style="color:#FF635C;">Lightsays</a></h1>
                <div class="nav-item active">
                    <a href="/article/tag/adv">推荐</a>
                </div>
                <div class="nav-item">
                    <a href="/article/tag/hot">热点</a>
                </div>
                <div class="nav-item">
                   <a href="/article/tag/video">视频</a>
                </div>
                <div class="nav-item">
                   <a href="/article/tag/picture">图片</a>
                </div>
                <div class="nav-item">
                   <a href="/article/tag/episode">段子</a>
               </div>
                <div class="nav-item">
                   <a href="/article/tag/society">社会</a>
               </div>
                <div class="nav-item">
                   <a href=/article/tag/amusement>娱乐</a>
               </div>
                <div class="nav-item">
                   <a href="/article/tag/gif">趣图</a>
               </div>
               <div class="nav-item">
                   <a href="/article/tag/technology">科技</a>
               </div>
                <div class="nav-item">
                   <a href="/article/tag/sports">体育</a>
               </div>
                <div class="nav-item">
                   <a href="/article/tag/car">汽车</a>
               </div>
               <div class="nav-item popup-more">
                  <a class="">更多</a>
               </div>
                <div class="more-popup ui segment" style="display:none;position:absolute;top:200px;left:100px;width:500px;z-index:-1;">
                    <div class="nav-item">
                       <a href="#">视频</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">图片</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">段子</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">社会</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">娱乐</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">趣图</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">科技</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">体育</a>
                    </div>
                    <div class="nav-item">
                       <a href="#">汽车</a>
                    </div>
                </div>
           </div>

        </div>
        <div class="ten wide column computer only">
            <!--banner star-->
            <div class="row">
                <div class="column">
                    @include('media.index.adv')
                </div>
            </div>
            <!--banner end-->
            <div class="ui hidden divider"></div>
            <div class="row">
                {{--@include('media.index.items')--}}
                <div class="ui divided items">
                    @foreach($articles as $article)
                        <div class="item">
                            <div class="ui small image">
                                <a href="{{ url('article/'.$article->id) }}"><img src="{{ $article->cover1 }}" alt="" width="120"></a>
                            </div>
                            <div class="content">
                                <a href="{{ url('article/'.$article->id) }}" class="header"><img src="{{ $article->cover1  }}" alt="" class="ui avatar image">
                                    {{ $article->title }}
                                </a>
                                <div class="description">
                                    <p>
                                        <span style="color:grey;margin: 0 .5em"> {{ articleCategory($article->content_type) }}</span>
                                        <span style="color:grey;margin: 0 .5em"> {{ $article->created_at }}</span>
                                    </p>

                                </div>
                                <div class="extra">
                                    <p>

                                        <span style="margin: 0 .8em"><i class="grey eye icon"></i>阅读:121</span>
                                        <span style="margin: 0 .8em"><i class="grey heart empty icon"></i>收藏:121</span>
                                        <span style="margin: 0 .8em"><i class="grey share alternate icon"></i>转发:121</span>
                                        <span style="margin: 0 .8em"><i class="grey comment outline icon"></i>评论:121</span>

                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!--items-->

        </div>
        <div class="four wide column computer only">
            <div class="right_wrapper_pin">
                <div class="search_pin">
                    <h2 class="page-header">站内搜索</h2>
                    <form action="/search" method="get">
                        <div class="ui action input">
                            <input type="text" name="query" placeholder="Search..">
                            <button class="ui red button" type="submit">Search</button>
                        </div>
                    </form>
                </div>
                {{----}}
                @include('media.index.nav')
                @include('media.index.hot_article')
                @include('media.index.hot_picture')
                @include('media.index.hot_video')
            </div>
        </div>
    </div>
    <div class="ui grid" style="overflow:hidden">
        <div class="sixteen wide column mobile only">
                <div class="ui divided items">
                    @foreach($articles as $article)
                        <div class="item">
                            <div class="ui image">
                                <a href="article/{{$article->id}}"><img src="{{ $article->cover1 }}" alt=""></a>
                            </div>
                            <div class="content">
                                <a href="article/{{$article->id}}" class="header"><img src="{{ $article->cover1 }}" alt="" class="ui avatar image">
                                    {{ $article->title }}
                                </a>
                                <div class="description">
                                    <p>
                                        <span style="color:grey;margin: 0 .5em"> {{ articleCategory($article->content_type) }}</span>
                                        <span style="color:grey;margin: 0 .5em"> {{ $article->created_at }}</span>
                                    </p>

                                </div>
                                <div class="extra">
                                    <p>
                                        <span style="margin: 0 .8em"><i class="grey eye icon"></i>阅读:121</span>
                                        <span style="margin: 0 .8em"><i class="grey heart empty icon"></i>收藏:121</span>
                                        <span style="margin: 0 .8em"><i class="grey comment outline icon"></i>评论:121</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
        </div>
    </div>
@endsection
@section('js')
    @parent
    <script src="//cdn.bootcss.com/jquery.pin/1.0.1/jquery.pin.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function( $ ) {
            $( '#example3' ).sliderPro({
                width: 700,
                height:370,
                fade: true,
                arrows: false,
                buttons: false,
                fullScreen: false,
                shuffle: true,
                smallSize: 500,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: false,
                autoplay: true
            });
        });
    </script>
    <script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".sidebar_pin").pin({
            containerSelector: ".navbar_wrapper_pin"
        })
        $(".popup-more").click(function() {
            $(".more-popup").fadeIn();
        });
    });
    </script>
    <script type="text/javascript">
        $(".sp-slide").click(function() {
            layer.msg(1);
        });
    </script>
@endsection
