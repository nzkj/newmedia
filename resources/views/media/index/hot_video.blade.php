<h2 class="page-header">热门视频</h2>
<div class="ui items">
    @foreach($hot_articles as $hot_article)
        <div class="item">
            <div class="ui tiny image">
                <img src="{{ $hot_article->cover1 }}" alt="">
            </div>
            <div class="middle aligned content">
                <h6 class="ui header">
                    <a href="{{ url('article/'.$hot_article->id) }}"  style="color: #000;">
                        {{ $hot_article->title }}
                    </a>
                </h6>
            </div>
        </div>
    @endforeach
</div>
