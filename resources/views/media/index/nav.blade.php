    <h2 class="page-header">内容分类</h2>
    <div class="ui labels">
        <a href="/article/tag/adv" class="ui red label">推荐</a>
        <a href="/article/tag/hot" class="ui label">热点</a>
        @foreach (articleCategory('array') as $k=>$v)
        <a href="/article/tag/{{ $k }}" class="ui label">{{ $v }}</a>

        @endforeach
        <a href="/article/tag/more" class="ui label">更多</a>
    </div>
