    <div id="example3" class="slider-pro">
        <div class="sp-slides">
            @foreach ($slides as $slide)
                <div class="sp-slide">
                    <a href="{{ $slide->url }}">
                        <img data-url="{{ $slide->url }}" class="sp-image" src="{{ $slide->cover }}"
                             data-src="{{ $slide->cover }}"
                             data-small="{{ $slide->cover }}"
                             data-medium="{{ $slide->cover }}"
                             data-large="{{ $slide->cover }}"
                             data-retina="{{ $slide->cover }}"/>
                    </a>
                    @if ($slide->abstract)
                        <h3 class="sp-layer sp-black sp-padding"
                            data-position="bottomLeft" data-horizontal="0" data-width="100%" data-show-transition="up"
                            data-show-delay="300" data-hide-transition="right">
                            {{ $slide->abstract }}
                        </h3>
                    @endif
                </div>
            @endforeach
        </div>
        <div class="sp-thumbnails">
            @foreach ($slides as $slide)
                <img data-url="{{ $slide->url }}" class="sp-thumbnail" src="{{ $slide->thumb_cover }}"/>
            @endforeach
        </div>
    </div>
