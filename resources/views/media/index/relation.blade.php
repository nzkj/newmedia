<h2 class="page-header">相关文章</h2>
<div class="ui items">
    @foreach($hot_articles as $hot_article)
        <div class="item">
            <div class="ui tiny image">
                <img src="{{ $hot_article->cover1 }}" alt="">
            </div>
            <div class="middle aligned content">
                <h5 class="ui header">
                    <a href="{{ url('article'.$hot_article->id) }}">
                        {{ $hot_article->title }}
                    </a>
                </h5>
            </div>
        </div>
    @endforeach
</div>