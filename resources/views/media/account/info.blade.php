@extends('media.layout.layout')
@section('css')
    @parent
    <style>
        h3>a{
            font-size: 18px;
            color: grey;
        }
        h3>a:hover,.ui.list>.item>a:hover{
            color: #FF635C;
        }
        .ui.list>.item{
            margin: .8em;
        }
        .ui.list>.item>a{
            font-size: 16px;
            color: black;

        }
        .ui.form .inline.field > label{
            width: 200px;
            font-size: 15px;
            color:#615D5D;
        }
        .ui.form .inline.field > span{
            color: grey;
        }
    </style>
@endsection

@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column" style="padding:1em 3em">
            <div class="ui secondary pointing borderless menu">
                <div class="item"><i class="icon setting"></i>设置</div>
                <div class="right menu">
                    <a class="active item" href="/account_info">基本信息</a>
                    <a class="item" href="/account_api">接口设置</a>
                </div>
            </div>
            <form class="ui form" action="index.html" method="post">
                <div class="inline field">
                    <label for="">轻说账户名称</label>
                    <span>南哲科技微讯</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline  field">
                    <label for="">轻说账户介绍</label>
                    <span>科技新资讯，时尚新潮流</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">轻说账户ID</label>
                    <span>ID:1</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">轻说账户头像</label>
                        <img src="{{ Auth::user()->avatar }}" alt="" class="ui small image" style="margin:1em 17em;">
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">轻说作者二维码</label>
                    <img src="{{ Auth::user()->avatar }}" alt="" class="ui small image" style="margin:1em 17em;">
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">联系人</label>
                    <span>赵文强</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">联系电话</label>
                    <span><i class="icon phone"></i>13143410521(China/中国）</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">领域</label>
                    <span>科技</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">所在地</label>
                    <span><i class="icon marker"></i>中国/广东/深圳</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">网站</label>
                    <span><i class="icon world"></i>http://sznzkj.cn</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">机构/公司名称</label>
                    <span>深圳市南哲科技有限公司</span>
                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">公司机构地址</label>
                    <span><i class="icon marker"></i>深圳市南山区粤海街道</span>

                    <input type="hidden"  name="" value="">
                </div>
                <div class="inline field">
                    <label for="">联系邮箱</label>
                    <span><i class="icon mail"></i>mankong@sznzkj.cn</span>
                    <input type="hidden"  name="" value="">
                </div>
                <button type="button" name="button" class="ui large red button" style="margin-left:220px;width:100px;">保存</button>
            </form>
        </div>
    </div>
@endsection
