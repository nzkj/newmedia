@extends('media.layout.layout')

@section('css')
    @parent
    <style>
        h3>a{
            font-size: 18px;
            color: grey;
        }
        h3>a:hover,.ui.list>.item>a:hover{
            color: #FF635C;
        }
        .ui.list>.item{
            margin: .8em;
        }
        .ui.list>.item>a{
            font-size: 16px;
            color: black;

        }
    </style>
@endsection

@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column" style="padding :1em 3em">
            <div class="ui secondary pointing borderless menu">
                <div class="active item">评论管理</div>
                <div class="right menu">
                    <div class="item">全部</div>
                    <div class="item">待回复</div>
                    <div class="item">已回复</div>
                </div>
            </div>
            <div class="ui four statistics">
                <div class="statistic">
                    <div class="value">
                        22
                    </div>
                    <div class="label">
                        关注
                    </div>
                </div>
                <div class="statistic">
                    <div class="value">
                        22
                    </div>
                    <div class="label">
                        粉丝
                    </div>
                </div>
                <div class="statistic">
                    <div class="value">
                        22
                    </div>
                    <div class="label">
                        推荐
                    </div>
                </div>
                <div class="statistic">
                    <div class="value">
                        31,200
                    </div>
                    <div class="label">
                        阅读
                    </div>
                </div>
            </div>
            <h4 class="ui header">文章</h4>
            <div class="ui list">
                @foreach(jzHotArticles() as $hots)
                    <div class="item"><a href="/article/{{$hots->id}}">{{ $hots->title }}</a></div>
                @endforeach
            </div>

            <div class="ui right floated horizontal list">
  <div class="disabled item" href="#">© GitHub, Inc.</div>
  <a class="item" href="#">Terms</a>
  <a class="item" href="#">Privacy</a>
  <a class="item" href="#">Contact</a>
</div>
<div class="ui horizontal list">
  <a class="item" href="#">About Us</a>
  <a class="item" href="#">Jobs</a>
</div>
        </div>
    </div>
@endsection
