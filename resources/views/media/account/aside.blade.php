<h1 class="ui header"><a href="/" style="color:#FF635C;">Lightsays</a></h1>
<h3><a href="/account"><i class="icon home"></i>首页</a></h3>
<div class="ui divider"></div>
<h3><a href="/publish_article"><i class="icon edit"></i>发布</a></h3>
<div class="ui divider"></div>

<h3><a><i class="icon book"></i>文章管理</a></h3>
<div class="ui list" style="padding-left: 1.5em">
    <div class="item"><a href="/article_lists">更新文章</a></div>
    <div class="item"><a href="/comments_manager">评论管理</a></div>
</div>
{{-- <h3><a><i class="icon money"></i>悬赏</a></h3>
<div class="ui list" style="padding-left: 1.5em">
    <div class="item"><a href="/reward_article">我的悬赏</a></div>
    <div class="item"><a href="/my_reward">我的收益</a></div>
</div>
<h3><a><i class="icon money"></i>收益</a></h3>
<div class="ui list" style="padding-left: 1.5em">
    <div class="item"><a href="/reward_setting">收益设置</a></div>
    <div class="item"><a href="/">自营广告</a></div>
    <div class="item"><a href="/">广告平台</a></div>
</div>
<div class="ui divider"></div>
<h3><a><i class="icon bar chart"></i>数据统计</a></h3>
<div class="ui list" style="padding-left: 1.5em">
    <div class="item"><a href="/article_anlysis">文章分析</a></div>
    <div class="item"><a href="/users_anlysis">用户分析</a></div>
    <div class="item"><a href="/fans_anlysis">粉丝分析</a></div>
</div> --}}
<div class="ui divider"></div>
<h3><a><i class="icon setting"></i>设置</a></h3>
<div class="ui list" style="padding-left: 1.5em">
    <div class="item"><a href="/account_info">账号信息</a></div>
    {{--<div class="item"><a href="/account_status">账号状态</a></div>--}}
    {{--<div class="item"><a href="/account_setting">账号设置</a></div>--}}
    <div class="item"><a href="/account_api">接口设置</a></div>
</div>
