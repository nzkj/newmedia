@extends('media.layout.layout')

@section('css')
    @parent
    <style>
        h3>a{
            font-size: 18px;
            color: grey;
        }
        h3>a:hover,.ui.list>.item>a:hover{
            color: #FF635C;
        }
        .ui.list>.item{
            margin: .8em;
        }
        .ui.list>.item>a{
            font-size: 16px;
            color: black;

        }

    </style>
@endsection

@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column">
            <div class="ui secondary pointing borderless menu">
                <div class="active item">修改头像</div>
            </div>
            <div class="ui hidden divider"></div>
            <div class="ui hidden divider"></div>
            <div class="ui hidden divider"></div>
            <form action="{{ url('upload_avatar') }}" class="ui form avatar_form">
                {{ csrf_field() }}
                <div class="field">
                    <input type="file" name="avatar" style="display: none" id="select_avatar">
                    @if(Auth::user()->avatar == null || Auth::user()->avatar == '')
                        <img src="images/jenny.jpg" class="ui image bordered centered" id="avatar" width="120">
                    @else
                        <img src="{{ Auth::user()->avatar }}" class="ui image bordered centered" id="avatar" width="120">
                    @endif
                </div>
                <div style="text-align: center;">
                    <span style="color: grey;font-size: 12px;">点击自动上传(建议2M以下)</span>
                </div>
            </form>
        </div>
    </div>
    <script>
        $("#avatar").click(function () {
            $("#select_avatar").click();
        });

        $("#select_avatar").change(function () {
            if(!window.FormData) {
                alert('your brower is too old');
                return false;
            }
            var form = $("form.avatar_form");
            var url = form.attr("action");
            var data = new FormData($( ".avatar_form" )[0]);
            $.ajax({
                url:url,
                type:'post',
                data:data,
                processData: false,
                contentType: false,
                dataType:'json',
                success:function(result){
                    if (result.status = 1) {
                        $("#avatar").attr('src',result.url);
                        layer.open({
                            content:result.info,
                            skin:'msg',
                            time:2
                        });
                    } else if (result.status = 0) {
                        layer.open({
                            content:result.info,
                            skin:'msg',
                            time:2
                        });
                    }
                },
            })
        });
    </script>
@endsection