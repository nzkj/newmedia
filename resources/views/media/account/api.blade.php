@extends('media.layout.layout')
@section('css')
    @parent
    <style>
        h3>a{
            font-size: 18px;
            color: grey;
        }
        h3>a:hover,.ui.list>.item>a:hover{
            color: #FF635C;
        }
        .ui.list>.item{
            margin: .8em;
        }
        .ui.list>.item>a{
            font-size: 16px;
            color: black;

        }

    </style>
@endsection

@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column" style="padding:1em 3em">
            <div class="ui secondary pointing borderless menu">
                <div class="item"><i class="icon setting"></i>设置</div>
                <div class="right menu">
                    <a class="item" href="/account_info">基本信息</a>
                    <a class="active item" href="/account_api">接口设置</a>
                </div>
            </div>
            <div class="ui hidden divider">

            </div>
            <h3>今日头条推文接口
                <span style="float:right">
                    <button class="ui tiny button primary add_toutiao_api"><i class="icon plus"></i>添加</button>
                </span>
            </h3>
            @if ($toutiaos)
                <table class="ui table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>名称</th>
                            <th>api_info</th>
                            <th>授权信息</th>
                            <th>创建时间</th>
                            <th>授权</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($toutiaos as $toutiao)
                            <tr>
                                <td>{{ $toutiao->id }}</td>
                                <td>{{ $toutiao->display_name }}</td>
                                <td>
                                    <p>key:{{ jzShowSafely($toutiao->client_key) }}</p>
                                    <p>secret:{{ jzShowSafely($toutiao->client_secret) }}</p>
                                </td>
                                <td>
                                    @if ($toutiao->access_token)
                                        <p>access_token:{{  jzShowSafely($toutiao->access_token) }}</p>
                                        <p>过期时间：{{ date('Y-m-d H:i:s', $toutiao->expires_in)}}</p>
                                    @else
                                        <div class="ui label">
                                            <a class="link">未授权</a>
                                        </div>
                                    @endif

                                </td>
                                <td>{{ $timeAgo->inWords($toutiao->created_at) }}</td>
                                <td>
                                    @if ($toutiao->expires_in !=null || $toutiao->expires_in !='')
                                        @if ($toutiao->expires_in >time())
                                            <button type="" name="button" class="ui icon button authorized"><i class="icon setting"></i></button>
                                        @else
                                            <button type="" name="button" class="ui icon button"><i class="icon setting"></i>刷新</button>
                                        @endif
                                    @else
                                        <form class="ui form authorize_form" action="/toutiao_authorize" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="toutiao_id" value="{{ $toutiao->id }}">
                                            <button type="button" class="ui icon button is_ajax_post" data-form="authorize_form"><i class="icon setting"></i>授权</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
    <div class="ui small modal">
        <i class="icon close"></i>
        <div class="header">
            添加接口
        </div>
        <div class="content">
            <form class="ui form post_form" action="/toutiao_create_api" method="post">
                {{ csrf_field() }}
                <div class="field">
                    <label for="">display_name</label>
                    <input type="text" name="display_name" value="" placeholder="显示名称">
                </div>
                <div class="field">
                    <label for="">client_key</label>
                    <input type="text" name="client_key" value="" placeholder="client_key">
                </div>
                <div class="field">
                    <label for="">client_key</label>
                    <input type="text" name="client_secret" value="" placeholder="client_secret">
                </div>
                <button type="button" name="button" class="ui button is_ajax_post" data-form="post_form">保存</button>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $('.add_toutiao_api').click(function() {
            $('.ui.small.modal')
                .modal('show')
            ;
        });
        $(".authorized").click(function() {
            layer.msg('您已授权！');
        });
    </script>
    <script type="text/javascript">
        $('.is_ajax_post').click(function() {
            var form = $("form." + $(this).data('form'));
            var url = form.attr('action');
            var data = form.serialize();
            $.post(url, data, function(result) {
                result = $.parseJSON(result);
                if (result.status==1){
                    layer.msg(result.info);
                    if (result.url){
                        window.location.href = result.url;
                    }
                }
                if (result.status==0){
                    layer.msg(result.info);
                    if (result.url){
                        window.location.href = result.url;
                    }
                }
            });
        });
    </script>
@endsection
