<div class="ui comments" id="comments">
    <h3 class="ui dividing header">评论</h3>
@if($comments)
        @foreach($comments as $comment)
            <div class="comment">
                <a class="avatar">
                    <img src="{{ $comment->avatar }}">
                </a>
                <div class="content">
                    <a class="author">{{ $comment->author }}</a>
                    <div class="metadata">
                        <span class="date">{{ $timeAgo->inWords($comment->created_at) }}</span>
                    </div>
                    <div class="text">
                        {{ $comment->content }}
                    </div>
                    <div class="actions">
                        <a class="reply" data-uid="{{ $comment->uid }}">回复</a>
                        <a class="open_reply" data-uid="{{ $comment->uid }}" data-author="{{ $comment->author }}">展开</a>
                        <a class="open_reply" data-uid="{{ $comment->uid }}" ><i class="icon pointing right"></i></a>
                        <a class="open_reply" data-uid="{{ $comment->uid }}" ><i class="icon pointing down"></i></a>
                    </div>
                    <div id="get_reply_{{ $comment->id }}">

                    </div>
                    <form class="ui reply form reply_form" action="{{ url('comment_create') }}" style="display: none">
                        <div class="field">
                            <textarea name="content" rows="2"></textarea>
                        </div>
                        {{ csrf_field() }}
                        <input type="hidden" name="article_id" value="{{ $article->id }}">
                        <input type="hidden" name="reply_to" value="" class="reply_to">
                        <button type="button" name="button" class="ui mini primary button is_reply_post">回复</button>
                        <button type="button" name="button" class="ui mini button close_reply">取消</button>
                    </form>
                </div>
            </div>
        @endforeach
        <form class="ui reply form comment_form" action="{{ url('comment_create') }}">
            <div class="field">
                <textarea name="content"></textarea>
            </div>
            {{ csrf_field() }}
            <input type="hidden" name="article_id" value="{{ $article->id }}">
            <div class="ui blue labeled submit icon button is_comment_post">
                <i class="icon edit"></i>评论
            </div>
        </form>

    @else
        <form class="ui reply form">
            <div class="field">
                <textarea></textarea>
            </div>
            <div class="ui blue labeled submit icon button">
                <i class="icon edit"></i>评论
            </div>
        </form>
@endif
</div>
<script>
    $(".is_comment_post").click(function () {
        var form = $("form.comment_form");
        var url = form.attr('action');
        var data = form.serialize();
        $.post(url,data,function (result) {
            result = $.parseJSON(result);
            if (result.status==1){
                layer.msg(result.info);
            }
        })

    });
    $("a.reply").click(function () {
        $(this).parent('div').siblings("form").css('display','block');
        $("form.comment_form").css('display','none');
        var reply_to = $(this).data('uid');
        $(this).parent('div').siblings("form.reply_form").children('input.reply_to').val(reply_to);

    });
    $(".is_reply_post").click(function () {

        var form = $(this).parent('form.reply_form');
        var url = form.attr('action');
        var data = form.serialize();
        $.post(url,data,function (result) {
            result = $.parseJSON(result);
            if (result.status==1){
                layer.msg(result.info);
            }
        })

    })
    $(".open_reply").click(function () {
        var uid = $(this).data('uid');
        var author = $(this).data('author');
        var url = "{{ url('get_reply/') }}" + '/comment_uid/' + uid + '/reply_to/' + author;
        $("#get_reply_"+uid).load(url);
    });
    $(".close_reply").click(function () {
        $(this).parent('form.reply_form').css('display','none');
        $("form.comment_form").css('display','block');
    });
</script>
