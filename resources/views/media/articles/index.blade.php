@extends('media.layout.layout')
@section('css')
    @parent
    <style>
       p{
           font-size: 16px;
           line-height: 36px;
       }
        a.section{
            font-size: 16px;
        }
       .ui.breadcrumb a{
           color: grey;
       }
       .page-header{
           padding-bottom: 0.7em;
           position: relative;
           border-bottom: 1px solid #eee;
       }
       .page-header:after{
           position: absolute;
           content: "";
           width: 1em;
           height: 0.2em;
           left: 0;
           bottom: -1px;
           background-color: #FF635C;
           -webkit-transition:width 1s;
           transition: width 0.2s;
       }
       .page-header:hover:after{
           width: 5em;
       }
    </style>
@endsection
@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid computer only">
        <div class="two wide column">
            <h2><a style="color: #FF635C" href="/">Lightsays</a></h2>
        </div>
        <div class="seven wide column" style="margin-top: .5em;">
            <div class="ui breadcrumb">
                <a href="{{ url('/') }}" class="section">首页</a>
                <div class="divider"> / </div>
                <a class="section">新闻</a>
                <div class="divider"> / </div>
                <div class="active section" style="color: grey">正文</div>
            </div>
        </div>
        <div class="seven wide column">
            <div class="ui fluid category search" style="float: right">
                <div class="ui icon input">
                    <input type="text" placeholder="Search animals...">
                    <i class="search icon"></i>
                </div>
                <div class="results"></div>
            </div>
        </div>
    </div>
    <div class="ui divider"></div>
    <div class="ui container grid">
        <div class="two wide column computer only navbar_wrapper_pin">
            <div class="sidebar_pin">
                <h1><a href="#comments" style="color: #FF635C"><i class="icon edit"></i> 182</a></h1>
                <div class="ui divider"></div>
                <h1><a href="#" style="color: #243544"><i class="icon green wechat"></i> <small>微信</small></a></h1>
                <h1><a href="#" style="color: #243544"><i class="icon blue qq"></i> <small>空间</small></a></h1>
                <h1><a href="#" style="color: #243544"><i class="icon red weibo"></i> <small> 微博</small></a></h1>
            </div>
        </div>
        <div class="sixteen wide mobile ten wide computer column">
            <div class="row">
                <h1 style="margin: 0">{{ $article->title }}</h1>
                <p style="color: grey;margin: 0">
                    <small>
                        <span style="padding: .12em .5em; border: 1px solid grey;">原创</span>
                        <span style="margin: 0 .3em">南哲科技微讯</span>
                        <span style="margin: 0 .3em">{{ $article->created_at }}</span>
                    </small>
                </p>
                {!!  htmlspecialchars_decode($article->content)  !!}
            </div>
            <!--items-->
            {{-- @include('media.articles.comments') --}}
            <div class="row">
                @include('media.articles.comments_vue')
            </div>
        </div>
        <div class="four wide column computer only">
            @include('media.index.hot_article')
        </div>
    </div>
@endsection
@section('js')
    @parent
    <script src="//cdn.bootcss.com/jquery.pin/1.0.1/jquery.pin.min.js"></script>
    <script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".sidebar_pin").pin({
            containerSelector: ".navbar_wrapper_pin"
        })
    });
    </script>
@endsection
