@extends('media.layout.layout')

@section('css')
@parent
<link rel="stylesheet" href="/plugins/wangEdit/css/wangEditor.css">
<style>
    h3>a{
        font-size: 18px;
        color: grey;
    }
    h3>a:hover,.ui.list>.item>a:hover{
        color: #FF635C;
    }
    .ui.list>.item{
        margin: .8em;
    }
    .ui.list>.item>a{
        font-size: 16px;
        color: black;

    }
</style>
@endsection

@section('js')
@parent
<script src="/plugins/wangEdit/js/wangEditor.js"></script>

@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column">
            <div class="ui secondary pointing borderless menu">
                <div class="item"><i class="icon edit"></i>发布类型</div>
                <div class="right menu">
                    <a class="active item" href="/publish_article">发表文章</a>
                    <a class="item" href="/publish_video">视频</a>
                    <a class="item" href="/publish_pictures">图集</a>
                </div>
            </div>
            <form class="ui form post_form" action="{{ url('article_create') }}">
                {{ csrf_field() }}
                <div class="field">
                    <div class="ui large input" style="border-radius:none">
                        <input type="text" name="title" value="{{ $article->title }}">
                    </div>
                    <textarea id="editor" name="content" style="min-height: 600px">
                    {{ $article->content }}
                    </textarea>
                </div>
                <div class="ui hidden divider"></div>
                <div class="inline fields">
                    <label>封面</label>
                    <div class="field">
                        <div class="ui cover-one-pic radio checkbox">
                            <input type="radio" name="cover_type" value="1">
                            <label>单图模式</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui cover-three-pic radio checkbox">
                            <input type="radio" name="cover_type" value="3">
                            <label>三图模式（仅WIFI下）</label>
                        </div>
                    </div>
                </div>
                <div class="ui hidden divider"></div>
                <div class="fields">
                    <div class="ui field cover-field1" style="display: none;">
                        <div class="ui segment select-cover cover1" style="width: 100px;height: 100px;text-align: center;padding: 35px;" data-cover="cover1">
                            <i class="icon large grey plus"></i>
                        </div>
                        <img src="" alt="" width="100" style="display: none" id="cover1">
                        <input type="hidden" name="cover1" value="" class="input_cover1">
                        <p class="tools_cover1" style="display: none;text-align: center;color:grey;"><i class="icon trash"></i>删除</p>
                    </div>
                    <div class="ui field cover-field2" style="display: none;" >
                        <div class="ui segment  select-cover cover2" style="width: 100px;height: 100px;text-align: center;padding: 35px;" data-cover="cover2">
                            <i class="icon large grey plus"></i>
                        </div>
                        <img src="" alt="" width="100" style="display: none" id="cover2">
                        <input type="hidden" name="cover2" value="" class="input_cover2">
                        <p class="tools_cover2" style="display: none;text-align: center;color: grey"><i class="icon trash"></i>删除</p>
                    </div>
                    <div class="ui field cover-field3" style="display: none;">
                        <div class="ui segment  select-cover cover3" style="width: 100px;height: 100px;text-align: center;padding: 35px;" data-cover="cover3">
                            <i class="icon large grey plus"></i>
                        </div>
                        <img src="" alt="" width="100" style="display: none" id="cover3">
                        <input type="hidden" name="cover3" value="" class="input_cover3">
                        <p class="tools_cover3" style="display: none;text-align: center;color: grey"><i class="icon trash"></i>删除</p>
                    </div>
                </div>
                <div class="field">
                    <label>类型</label>
                    <select class="ui search dropdown" name="content_type">
                        @foreach(articleCategory('array') as $k => $v)
                            <option value="{{ $k }}">{{ $v }}</option>
                        @endforeach
                    </select>
                </div>
            </form>
            <div class="ui hidden divider"></div>
            <button class="ui large red button is_ajax_post" data-type="publish">发表</button>
            <button class="ui large button is_ajax_post" data-type="draft">存草稿</button>
            <button class="ui large button is_ajax_post" data-type="preview">预览</button>
            <button class="ui large button is_ajax_post" data-type="cannel">取消</button>

            <div class="ui small modal">
                <i class="close icon"></i>
                <div class="header">
                    设置封面图
                </div>
                <div class="content">
                    <form class="ui form" id="article_covers"></form>
                </div>
            </div>
            <footer style="min-height: 5em">

            </footer>
            <!--这里引用jquery和wangEditor.js-->
            <script type="text/javascript">
                var editor = new wangEditor('editor');
                editor.config.menuFixed = false;
                // 上传图片（举例）

                editor.config.uploadImgUrl = '/upload_picture';

                // 配置自定义参数（举例）
                editor.config.uploadParams = {
                    _token: "{{ csrf_token() }}",
                    user: 'wangfupeng1988'
                };

                // 设置 headers（举例）
                editor.config.uploadHeaders = {
                    'Accept' : 'text/x-json'
                };

                // 隐藏掉插入网络图片功能。该配置，只有在你正确配置了图片上传功能之后才可用。
                editor.config.hideLinkImg = true;
                editor.create();


                $('.ui.dropdown')
                        .dropdown('set selected' ,"{{ $article->content_type}}")
                ;
                $('.ui.checkbox')
                        .checkbox()
                ;


            </script>
            <script>
                //单图
                $('.ui.cover-one-pic.radio.checkbox')
                        .checkbox()
                        .checkbox({
                            onChecked: function() {
                                $("div.cover-field1").css('display','block');
                                $("div.cover-field2").css('display','none');
                                $("div.cover-field3").css('display','none');
                                $("input.input_cover2").val('');
                                $("input.input_cover3").val('');

                            }
                        })
                ;
                //多图
                $('.ui.cover-three-pic.radio.checkbox')
                        .checkbox()
                        .checkbox({
                            onChecked: function() {
                                $("div.cover-field1").css('display','block');
                                $("div.cover-field2").css('display','block');
                                $("div.cover-field3").css('display','block');
                            }
                        })
                ;
                $('.select-cover').click(function () {
                    var cover_id = $(this).data('cover');
                    localStorage.setItem("cover_id",cover_id);
                    var check_url = "{{  url('article_cover_check') }}"
                    $.get(check_url,function (result) {
                        result = $.parseJSON(result);
                        if(result.status==1){
                            var cover_url = "{{ url('article_covers') }}";
                            $("#article_covers").load(cover_url);

                            $('.ui.small.modal')
                                    .modal('show')
                            ;
                        }

                        if(result.status==0){
                            layer.msg(result.info);
                            return false;
                        }
                    })

                })
            </script>
            <script>
                $('.is_ajax_post').click(function () {
                    var type = $(this).data('type');
                    if (type=='draft'){
                        var url = "{{  url('article_draft') }}";
                    }
                    if (type=='preview'){
                        layer.msg('暂未开放');
                        return false;
                    }
                    if (type=='cannel'){
                        var url = "{{url('/')}}";
                        layer.msg('您取消了发布');
                        window.location.href = url;
                        return false;

                    }
                    var form = $('form.post_form');
                    if (type=='publish'){
                        var url = form.attr('action');
                    }
                    var data = form.serialize();
                    $.post(url,data,function (result) {
                        result = $.parseJSON(result);
                        if (result.status==1){
                            layer.msg(result.info);
                            if (result.url){
                                window.location.href = result.url;
                            }
                        }
                        if (result.status==0){
                            layer.msg(result.info);
                            if (result.url){
                                window.location.href = result.url;
                            }
                        }
                    });
                });
            </script>
        </div>
    </div>
@endsection
