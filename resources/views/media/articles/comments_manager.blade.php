@extends('media.layout.layout')

@section('css')
    @parent
    <style>
        h3>a{
            font-size: 18px;
            color: grey;
        }
        h3>a:hover,.ui.list>.item>a:hover{
            color: #FF635C;
        }
        .ui.list>.item{
            margin: .8em;
        }
        .ui.list>.item>a{
            font-size: 16px;
            color: black;

        }
    </style>
@endsection

@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column" style="padding:1em 3em">
            <div class="ui secondary pointing borderless menu">
                <div class="active item">评论管理</div>
                <div class="right menu">
                    <div class="item">全部</div>
                    <div class="item">待回复</div>
                    <div class="item">已回复</div>
                </div>
            </div>
            <div class="ui comments" id="comments">
                <h3 class="ui dividing header">Comments</h3>
                @if($comments)
                    @foreach($comments as $comment)
                        <div class="comment">
                            <a class="avatar">
                                <img src="{{ $comment->avatar }}">
                            </a>
                            <div class="content">
                                <a class="author">{{ $comment->author }}</a>
                                <div class="metadata">
                                    <span class="date">{{ $timeAgo->inWords($comment->created_at) }}</span>
                                </div>
                                <div class="text">
                                    {{ $comment->content }}
                                </div>
                                <div class="actions">
                                    <a class="reply" data-uid="{{ $comment->uid }}">回复</a>
                                    <a class="open_reply" data-uid="{{ $comment->uid }}" data-author="{{ $comment->author }}">展开</a>
                                </div>
                                <div id="get_reply_{{ $comment->id }}">

                                </div>
                                <form class="ui reply form reply_form" action="{{ url('comment_create') }}" style="display: none">
                                    <div class="field">
                                        <textarea name="content"></textarea>
                                    </div>
                                    {{ csrf_field() }}
                                    <input type="hidden" name="article_id" value="{{ $article->id }}">
                                    <input type="hidden" name="reply_to" value="" class="reply_to">
                                    <div class="ui blue labeled submit icon button is_reply_post">
                                        <i class="icon edit"></i>回复
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endforeach
                    <form class="ui reply form comment_form" action="{{ url('comment_create') }}">
                        <div class="field">
                            <textarea name="content"></textarea>
                        </div>
                        {{ csrf_field() }}
                        <input type="hidden" name="article_id" value="{{ $article->id }}">
                        <div class="ui blue labeled submit icon button is_comment_post">
                            <i class="icon edit"></i>评论
                        </div>
                    </form>

                @else
                    <form class="ui reply form">
                        <div class="field">
                            <textarea></textarea>
                        </div>
                        <div class="ui blue labeled submit icon button">
                            <i class="icon edit"></i>评论
                        </div>
                    </form>
                @endif
        </div>
    </div>
@endsection
