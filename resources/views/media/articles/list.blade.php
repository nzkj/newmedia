@extends('media.layout.layout')

@section('css')
    @parent
    <style>
        h3>a{
            font-size: 18px;
            color: grey;
        }
        h3>a:hover,.ui.list>.item>a:hover{
            color: #FF635C;
        }
        .ui.list>.item{
            margin: .8em;
        }
        .ui.list>.item>a{
            font-size: 16px;
            color: black;

        }
    </style>
@endsection

@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column" style="padding:1em 3em">
            <div class="ui secondary pointing borderless menu">
                <div class="active item">手动更新</div>
                <div class="right menu">
                    <div class="item">全部</div>
                    <div class="item">已发表</div>
                    <div class="item">审核中</div>
                    <div class="item">未通过</div>
                    <div class="item">草稿箱</div>
                </div>
            </div>
            <div class="ui divided items">
                @foreach($articles as $article)
                    <div class="item">
                        <div class="ui small image">
                            <img src="{{ $article->cover1 }}" alt="" width="120">
                        </div>

                        <div class="content">
                            <a href="{{url('article/'.$article->id )}}" class="header"><img src="{{ $article->cover1  }}" alt="" class="ui avatar image">
                                {{ $article->title }}
                            </a>
                            <div class="description">
                                <p>
                                    <a class="ui pink horizontal label" style="color:grey;margin: 0 .5em">已推荐</a>
                                    <span style="color:grey;margin: 0 .5em"> {{ articleCategory($article->content_type) }}</span>
                                    <span style="color:grey;margin: 0 .5em"> {{ $article->created_at }}</span>
                                    <span style="color:grey;margin: 0 .5em"> {{ $article->is_publish== 1 ? '已发表':'未发布' }}</span>
                                    <span style="color:grey;margin: 0 .5em"><a href="/article_edit/{{ $article->id }}"><i class="icon edit"></i>修改</a></span>
                                    <span style="color:grey;margin: 0 .3em"><a class="is_ajax_get" data-url="/article_uptop/{{ $article->id }}"><i class="icon upload"></i>置顶</a></span>
                                    <span style="color:grey;margin: 0 .3em"><a class="is_ajax_get" data-url="/article_delete/{{ $article->id }}"><i class="icon trash"></i>删除</a></span>
                                    {{-- <span style="color:grey;margin: 0 .3em"><a class="is_ajax_get" data-url="/toutiao_authorize"><i class="icon refresh"></i>推送</a></span> --}}
                                    <span style="color:grey;margin: 0 .3em"><a href="/article_push/{{ $article->id }}"><i class="icon refresh"></i>推送</a></span>
                                    <span style="color:grey;margin: 0 .3em"></span>
                                </p>

                            </div>
                            <div class="extra">
                                <p>

                                    <span style="margin: 0 .8em"><i class="grey rss icon"></i>推荐:1200</span>
                                    <span style="margin: 0 .8em"><i class="grey eye icon"></i>阅读:121</span>
                                    <span style="margin: 0 .8em"><i class="grey heart empty icon"></i>收藏:121</span>
                                    <span style="margin: 0 .8em"><i class="grey share alternate icon"></i>转发:121</span>
                                    <span style="margin: 0 .8em"><i class="grey comment outline icon"></i>评论:121</span>

                                </p>

                            </div>
                        </div>
                        <div class="ui image" style="position: relative;">
                            {!! QrCode::margin(0)->size(90)->generate(url('/article/'.$article->id)) !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="ui small modal">
        <i class="icon close"></i>
        <div class="header">

        </div>
        <div class="content">
            <h2>今日头条推送</h2>
            <form class="ui form post_form" action="https://mp.toutiao.com/open/new_article_post/" method="post">
                {{ csrf_field() }}
                <div class="inline fields">
                    <label for="">选择推送账户</label>
                    @if ($toutiaos =jzGetToutiaos(Auth::id()))
                        @foreach ($toutiaos as $toutiao)
                            <div class="field">
                                <div class="ui radio checkbox">
                                    <input type="radio" name="toutiao_id" value="{{ $toutiao->id }}">
                                    <label for="">{{ $toutiao->display_name}}</label>
                                </div>
                            </div>
                            <input type="hidden" name="access_token" value="{{ jzGetToutiaoAccessToken($toutiao->id) }}">
                        @endforeach
                    @endif
                </div>
                <button type="button" class="ui small button primary is_ajax_post" name="button">推送</button>
            </form>
            <div class="ui divider">

            </div>
            <h2>UC推送</h2>
            <p>暂无配置</p>
            <h2>网易推送</h2>
            <p>暂无配置</p>
            <h2>搜狐推送</h2>
            <p>暂无配置</p>
        </div>
    </div>
    <script type="text/javascript">
        $(".is_ajax_get").click(function() {
            var url = $(this).data('url');
            layer.confirm('确定进行该操作？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.get(url,function (result) {
                    result = $.parseJSON(result);
                    layer.msg(result.info);
                    if (result.url) {
                        window.location.href = result.url;
                    }
                })
            }, function(){
                layer.msg('已取消操作');
            });

        });
    </script>
    <script type="text/javascript">
        $('.open_push_article').click(function() {
            $('.ui.small.modal')
                .modal('show')
            ;
        });
        $('.ui.radio.checkbox')
            .checkbox({

            })
            ;
        $('.is_ajax_post').click(function() {
            var form = $("form.post_form");
            var url = form.attr('action');
            var data = form.serialize();
            $.post(url, data, function(result) {
                result = $.parseJSON(result);
                if (result.status==1){
                    layer.msg(result.info);
                    if (result.url){
                        window.location.href = result.url;
                    }
                }
                if (result.status==0){
                    layer.msg(result.info);
                    if (result.url){
                        window.location.href = result.url;
                    }
                }
            });
        });
    </script>
@endsection
