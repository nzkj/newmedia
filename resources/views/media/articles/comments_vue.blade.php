<div id="comments">
    <comment-item :items="comments" :show_reply="show_reply" :show_comment="show_comment"></comment-item>
    <button type="button" class="ui mini fluid button" @click="get_more_comments">加载更多</button>
</div>
<template id="comment-item">
<div class="ui comments">
    <h3 class="ui dividing header"></h3>
    <form v-if="show_comment" class="ui reply form comment_form" action="{{ url('comment_create') }}">
        <div class="field">
            <textarea name="content" style="height:2em" v-model="comment_content"></textarea>
        </div>
        {{ csrf_field() }}
        <input type="hidden" name="article_id" value="{{ $article->id }}">
        <a type="button" @click="post_comment" class="ui centered mini primary button" style="float:right">评论</a>
    </form>
    <h3 class="ui dividing header">评论<small><span style="color:grey">&nbsp;@{{comment_content}}</span></small></h3>
    <div style="clear:both"></div>
    <div class="comment" v-for="(item,index) in items">
        <a class="avatar">
            <img :src="item.avatar">
        </a>
        <div class="content">
            <a class="author">@{{ item.author }}</a>
            <div class="metadata">
                <span class="date">@{{ item.friendly_time }}</span>
            </div>
            <div class="text">
                @{{ item.content }}
            </div>
            <div class="actions">
                <a class="reply" @click="reply(item.id,item.user_id,item.author,index)">回复</a>
                <a class="reply" @click="get_comment_reply(item.id,index)">展开</a>
                <a class="open_reply" ><i class="icon pointing right"></i></a>
                <a class="open_reply"><i class="icon pointing down"></i></a>
            </div>
            <comment-reply></comment-reply>
        </div>
    </div>
</div>
</template>
<template id="comment-reply">
    <div class="">
        <form v-show="show_comment_reply" class="ui reply form reply_form" action="" style="display: none;">
            <div class="field">
                <textarea name="content" style="height:2em" v-model="reply_content"></textarea>
            </div>
            {{ csrf_field() }}
            <button type="button" @click="post_reply" class="ui mini primary button">回复</button>
            <button type="button" @click="close_comment_reply" class="ui mini button">取消</button>
        </form>
        <div v-if="replys.length" class="comments">
            <div class="comment" v-for="reply in replys">
                <a class="avatar">
                    <img :src="reply.avatar">
                </a>
                <div class="content">
                    <a class="author">@{{ reply.author }} 回复 @//</a>
                    <div class="metadata">
                        <span class="date">@{{ reply.friendly_time }}</span></span>
                    </div>
                    <div class="text">
                        @{{ reply.content }}
                    </div>
                    <div class="actions">
                        <a class="reply" @click="reply_reply(reply.id,reply.user_id,reply.author)">回复</a>
                        <a class="open_reply" ><i class="icon pointing right"></i></a>
                        <a class="open_reply"><i class="icon pointing down"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
<script src="/plugins/vue/vue.js"></script>
<script src="//cdn.bootcss.com/vue-resource/1.0.3/vue-resource.js"></script>
<script>
    Vue.component('comment-item',{
        template: '#comment-item',
            props: {
                items: Array,
                show_reply:false,
                show_comment:true,
            },
            methods:{
                //回复窗口
                reply:function (comment_id,user_id,author,index) {
                    this.$children[index].show_comment_reply = !this.$children[index].show_comment_reply;
                    this.$root.show_comment =  !this.$root.show_comment;
                    this.$children[index].reply_to = comment_id;
                    this.$children[index].reply_to_user_id = user_id;
                    this.$children[index].reply_content = '回复@'+author+'//';
                },
                get_comment_reply:function (comment_id,index) {
                    this.$http.get('/api/get_comment_replys?comment_id='+comment_id).then(response=>{
                        console.log(index);
                        this.$children[index].replys = response.body;
                    }, function () {
                        layer.msg('error')
                    })
                },
                post_comment:function () {
                    if("{{Auth::check()}}"==0){
                        layer.confirm('您还未登录，是否去登录？', {
                          btn: ['确定','取消'] //按钮
                        }, function(){
                             layer.msg('正在跳转...');
                            window.location.href = "/login";
                        }, function(){
                          layer.msg('也可以这样', {
                            time: 20000, //20s后自动关闭
                            btn: ['明白了', '知道了']
                          });
                        })
                        return false;
                    }else{
                        if (this.comment_content=='') {
                            layer.msg('评论不能为空');
                            return false;
                        }
                        const postData = {
                            'content':this.comment_content,
                            'article_id': "{{ $article->id }}",
                            'user_id': "{{ Auth::id() }}",
                            'type':'comment',
                            'status':1
                        }

                        this.$http.post('/api/post_comment',postData).then(response=>{
                            console.log(response);
                            this.$root.comments.unshift(response.body);
                            this.comment_content = '';
                        }, function () {

                        })
                    }

                }
            },
            data:function () {
                return {
                    comment_content:'',
                }
            }
    })
    Vue.component('comment-reply',{
        template: '#comment-reply',
        data:function () {
            return{
                show_comment_reply:false,
                reply_content:'',
                reply_to:'',
                reply_to_user_id:'',
                reply_author:'',
                replys:[]
            }
        },
        methods: {
            reply_reply:function (reply_id,reply_to_user_id,author) {
                this.show_comment_reply = !this.show_comment_reply;
                this.$root.show_comment =  !this.$root.show_comment;
                this.reply_to = reply_id;
                this.reply_to_user_id = reply_to_user_id;
                this.reply_content = '回复@'+author+'//';
            },
            close_comment_reply:function () {
                this.show_comment_reply = false;
                this.$root.show_comment =  true;

            },
            post_reply:function () {
                if("{{Auth::check()}}"==0){
                    layer.confirm('您还未登录，是否去登录？', {
                      btn: ['确定','取消'] //按钮
                    }, function(){
                         layer.msg('正在跳转...');
                        window.location.href = "/login";
                    }, function(){
                      layer.msg('也可以这样', {
                        time: 20000, //20s后自动关闭
                        btn: ['明白了', '知道了']
                      });
                    })
                    return false;
                }else{
                    if (this.reply_content=='') {
                        layer.msg('回复不能为空');
                        return false;
                    }
                    const postData = {
                        'content':this.reply_content,
                        'article_id': "{{ $article->id }}",
                        'user_id': "{{ Auth::id() }}",
                        'reply_to':this.reply_to ,
                        'reply_to_user_id':this.reply_to_user_id ,
                        'type':'reply',
                        'status':1
                    }

                    this.$http.post('/api/post_comment',postData).then(response=>{
                        console.log(response);
                        this.replys.unshift(response.body);
                        this.reply_content = '';
                        this.show_comment_reply =false;
                    })
                }
            }

        }
    })
    var vm = new Vue({
        el:'#comments',
        data:{
            show_reply:false,
            show_comment:true,
            comments:[],
            last_id :0,

        },
        methods:{
            get_more_comments:function () {
                article_id = "{{ $article->id }}";
                last_id = this.comments[this.comments.length-1].id;
                console.log(last_id);
                this.$http.get('/api/get_more_comments?article_id='+article_id+'&last_id='+last_id)
                .then(response=>{
                    if (response.body.length==0) {
                        layer.msg('已加载所有评论没有更多')
                    }else{
                        this.comments = this.comments.concat( response.body );
                    }
                })
            }
        },
        created () {
            article_id = "{{ $article->id }}";
            this.$http.get('/api/get_article_comments?article_id='+article_id).then( response=>{
                this.comments = response.body;
            })
        }

    })
</script>
