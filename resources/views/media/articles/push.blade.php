@extends('media.layout.layout')

@section('css')
@parent
<link rel="stylesheet" href="/plugins/wangEdit/css/wangEditor.css">
<style>
    h3>a{
        font-size: 18px;
        color: grey;
    }
    h3>a:hover,.ui.list>.item>a:hover{
        color: #FF635C;
    }
    .ui.list>.item{
        margin: .8em;
    }
    .ui.list>.item>a{
        font-size: 16px;
        color: black;

    }
</style>
@endsection

@section('js')
@parent
<script src="/plugins/wangEdit/js/wangEditor.js"></script>

@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column">
            <div class="ui secondary pointing borderless menu">
                <div class="item"><i class="icon edit"></i>推送</div>
                <div class="right menu">
                    <a class="active item" href="/publish_article">推送文章</a>
                </div>
            </div>

            <form class="ui form post_form" action="{{ url('article_create') }}">
                {{ csrf_field() }}
                <div class="two fields">
                    <div class="field">
                        <label>标题</label>
                        <input type="text" name="title" value="{{ $article->title }}">
                    </div>
                    <div class="field">
                        <label>类型</label>
                        <select class="ui search dropdown" name="article_tag">
                            @foreach(jzToutiaoCategorys('array') as $k => $v)
                                <option value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <div class="ui red segment">
                            <h2 class="ui header">{{$article->title}}</h2>
                        {!!  htmlspecialchars_decode($article->content)  !!}
                        </div>
                        <textarea id="editor" name="content" style="min-height: 600px">
                        {{ $article->content }}
                        </textarea>
                    </div>
                    <div class="field">
                        <div class="field">
                            <label for="">文章摘要</label>
                            <textarea name="abstract" rows="8" cols="80"></textarea>
                        </div>
                        <div class="ui hidden divider"></div>
                        <img src="/images/toutiao.png" alt="" class="ui tiny image">
                        <div class="ui hidden divider"></div>
                        <div class="inline fields">
                            <label for="">推送账户</label>
                            @if ($toutiaos =jzGetToutiaos(Auth::id()))
                                @foreach ($toutiaos as $toutiao)
                                    <div class="field">
                                        <div class="ui toutiao radio checkbox">
                                            <input type="radio" name="toutiao_id" value="{{ $toutiao->id }}" data-key="{{$toutiao->client_key}}" data-token="{{ $toutiao->access_token }}">
                                            <label for="">{{ $toutiao->display_name}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                        <input type="hidden" name="client_key" value="{{ $toutiao->client_key }}" id="client_key">
                        <input type="hidden" name="access_token" value="{{ $toutiao->access_token }}" id="access_token">
                        <div class="inline fields">
                            <label>保存方式</label>
                            <div class="field">
                              <div class="ui radio checkbox">
                                <input type="radio" name="save" value="0" checked="checked">
                                <label>存草稿</label>
                              </div>
                            </div>
                            <div class="field">
                              <div class="ui radio checkbox">
                                <input type="radio" name="save" value="1">
                                <label>发布</label>
                              </div>
                            </div>
                          </div>
                        <button type="button" name="button" class="ui primary fluid button">推送</button>
                    </div>
                </div>


            </form>
            <footer style="min-height: 5em">

            </footer>
            <!--这里引用jquery和wangEditor.js-->
            <script type="text/javascript">
                var editor = new wangEditor('editor');
                editor.config.menuFixed = false;
                // 上传图片（举例）
                editor.config.menus = [
                    'source',
                    '|',
                    'bold',
                    'underline',
                    'italic',
                    'strikethrough',
                    'eraser',
                    'forecolor',
                    'bgcolor'
                 ];
                editor.config.uploadImgUrl = '/upload_picture';

                // 配置自定义参数（举例）
                editor.config.uploadParams = {
                    _token: "{{ csrf_token() }}",
                    user: 'wangfupeng1988'
                };

                // 设置 headers（举例）
                editor.config.uploadHeaders = {
                    'Accept' : 'text/x-json'
                };

                // 隐藏掉插入网络图片功能。该配置，只有在你正确配置了图片上传功能之后才可用。
                editor.config.hideLinkImg = true;
                editor.create();

                $('.ui.checkbox')
                    .checkbox()
                ;
                $('.ui.toutiao.radio.checkbox')
                    .checkbox({
                        onChecked:function () {
                            key = $(this).data('key');
                            access_token = $(this).data('token');
                            console.log(key);
                            console.log(access_token);
                        }
                    })
                ;

            </script>

            <script>
                $('.is_ajax_post').click(function () {
                    var type = $(this).data('type');
                    if (type=='draft'){
                        var url = "{{  url('article_draft') }}";
                    }
                    if (type=='preview'){
                        layer.msg('暂未开放');
                        return false;
                    }
                    if (type=='cannel'){
                        var url = "{{url('/')}}";
                        layer.msg('您取消了发布');
                        window.location.href = url;
                        return false;

                    }
                    var form = $('form.post_form');
                    if (type=='publish'){
                        var url = form.attr('action');
                    }
                    var data = form.serialize();
                    $.post(url,data,function (result) {
                        result = $.parseJSON(result);
                        if (result.status==1){
                            layer.msg(result.info);
                            if (result.url){
                                window.location.href = result.url;
                            }
                        }
                        if (result.status==0){
                            layer.msg(result.info);
                            if (result.url){
                                window.location.href = result.url;
                            }
                        }
                    });
                });
            </script>
        </div>

    </div>
@endsection
