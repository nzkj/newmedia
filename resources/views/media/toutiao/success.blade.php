@extends('media.layout.layout')
@section('css')
    @parent
    <style>
        h3>a{
            font-size: 18px;
            color: grey;
        }
        h3>a:hover,.ui.list>.item>a:hover{
            color: #FF635C;
        }
        .ui.list>.item{
            margin: .8em;
        }
        .ui.list>.item>a{
            font-size: 16px;
            color: black;

        }

    </style>
@endsection

@section('js')
    @parent
@endsection

@section('main')
    <div class="ui container grid">
        <div class="three wide column">
            @include('media.account.aside')
        </div>
        <div class="thirteen wide column" style="padding:1em 3em">
            <div class="ui secondary pointing borderless menu">
                <div class="item"><i class="icon setting"></i>设置</div>
                <div class="right menu">
                    <a class="item" href="/account_info">基本信息</a>
                    <a class="active item" href="/account_api">接口设置</a>
                </div>
            </div>
            <div class="ui hidden divider">

            </div>
            <h1>授权成功</h1>

        </div>
    </div>
    <script type="text/javascript">
        $('.is_ajax_post').click(function() {
            var form = $("form.post_form");
            var url = form.attr('action');
            var data = form.serialize();
            $.post(url, data, function(result) {
                result = $.parseJSON(result);
                if (result.status==1){
                    layer.msg(result.info);
                    if (result.url){
                        window.location.href = result.url;
                    }
                }
                if (result.status==0){
                    layer.msg(result.info);
                    if (result.url){
                        window.location.href = result.url;
                    }
                }
            });
        });
    </script>
@endsection
