<!doctype html>
<html lang="en">
<head>
    @include('media.public.meta')
    @include('media.public.css')
    @include('media.public.js')
    <title>@yield('metatitle')</title>
</head>
<body>
<div class="pusher">
    @include('media.public.menu')
    @section('main')@show
</div>
</body>
</html>
