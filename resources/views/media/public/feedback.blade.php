@extends('media.layout.layout')

@section('css')
    @parent

@endsection

@section('js')
    @parent
@endsection

@section('main')

<div class="ui grid container" style="padding:7em 15em">
    <form class="ui form" action="/feedback" method="post">
        <div class="field">
            <label for="">主题</label>
            <input type="text" name="subject" value="">
        </div>
        <div class="field">
            <label for="">内容</label>
            <textarea name="name" rows="8" cols="80"></textarea>
        </div>
        <button type="button" name="button" class="ui fluid primary button">提交</button>
    </form>
    {{-- <h5>您的关注使我们成长的动力！</h5> --}}
</div>
@endsection
