@section('menu')
<div class="ui inverted menu " style="border-radius: 0">
    <a class="item" href="/">轻说</a>
    <div class="right menu">
        @if(Auth::check())
            <a class="item" href="/publish_article">发文</a>
            <a class="item" href="/account" style="padding: 0 1.14285714em;">
                <img src="{{ Auth::user()->avatar }}" alt="" class="ui avatar image">
                {{ Auth::user()->name }}
            </a>
        @else
            <a class="item" href="/login">登录</a>
            <a class="item" href="/account">轻说号</a>
        @endif
        {{-- <a class="item" href="/feedback">反馈</a>
        <a class="item" href="/cooperate">合作</a> --}}
        <div class="ui dropdown item">
        更多
        <i class="dropdown icon"></i>
        <div class="menu">
            <a class="item" href="/more">内容导航</a>
            <div class="item">关于轻说</div>
            <a href="/register" class="item">加入轻说</a>
            <div class="item">用户协议</div>
            <div class="item">联系我们</div>
            @if (Auth::check())
                <a class="item" href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    退出登录
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
        </div>
      </div>
    </div>
</div>
@show
