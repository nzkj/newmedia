<div class="comments">
    @foreach($replys as $reply)
        <div class="comment">
            <a class="avatar">
                <img src="{{ $reply->avatar }}">
            </a>
            <div class="content">
                <a class="author">{{ $reply->author }} 回复 @// {{ $author }}</a>
                <div class="metadata">
                    <span class="date">{{ $timeAgo->inWords($reply->created_at) }}</span></span>
                </div>
                <div class="text">
                    {{ $reply->content }}
                </div>
                <div class="actions">
                    <a class="reply">Reply</a>
                </div>
            </div>
        </div>
    @endforeach
</div>