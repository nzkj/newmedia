<div class="fields">
@foreach($article_pictures  as $picture)
<div class="field">
    <a>
        <img src="{{ $picture }}" alt="" width="100" class="add_cover" data-url="{{ $picture }}">
    </a>
</div>
@endforeach
</div>
<script>
    $(".add_cover").click(function () {
        var url = $(this).data('url');
        var cover_id = localStorage.getItem('cover_id');
        localStorage.setItem(cover_id,url);
        $("input.input_"+cover_id).val(url);
        $("."+cover_id).css('display','none');
        $("#"+cover_id).attr('src',url);
        $("#"+cover_id).css('display','block');
        $(".tools_"+cover_id).css('display','block');
        $('.ui.small.modal')
                .modal('hide')
        ;
    })

    $(".tools_cover1").click(function () {
        localStorage.removeItem('cover1');
        $("input.input_cover1").val('');
        $('.tools_cover1').css('display','none');
        $("#cover1").css('display','none');
        $('.cover1').css('display','block');

    });
    $(".tools_cover2").click(function () {
        localStorage.removeItem('cover2');
        $("input.input_cover2").val('');
        $('.tools_cover2').css('display','none');
        $("#cover2").css('display','none');
        $('.cover2').css('display','block');
    });
    $(".tools_cover3").click(function () {
        localStorage.removeItem('cover3');
        $("input.input_cover3").val('');
        $('.tools_cover3').css('display','none');
        $("#cover3").css('display','none');
        $('.cover3').css('display','block');
    });
</script>