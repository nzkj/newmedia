<?php
function articleCategory($content_type)
{
    $data = [
        'video'=>'视频',
        'picture'=>'图片',
        'episode'=>'段子',
        'society'=>'社会',
        'amusement'=>'娱乐',
        'gif'=>'趣图',
        'technology'=>'科技',
        'sports'=>'体育',
        'car'=>'汽车',
        'finance'=>'财经',
        'military'=>'军事',
        'international'=>'国际',
        'estate'=>'房产',
        'daren'=>'达人',
        'meiwen'=>'美文',
        'beauty'=>'美女',
        'sex'=>'两性',
        'health'=>'健康',
        'gongyi'=>'公益',
        'ad'=>'广告'
    ];
    if ($content_type=='key') {
        return array_keys($data);
    }
    if ($content_type=='value') {
        return array_values($data);
    }
    if ($content_type=='array') {
        return $data;
    }
    if (array_key_exists($content_type, $data)) {
        return $data[$content_type];
    }
}

/**
 * 热门文章
 * @return [type] [description]
 */
function jzHotArticles()
{
    if (\Cache::has('hot_articles')) {
        $hot_articles = Cache::get('hot_articles');
    } else {
        $hot_articles = \DB::table('articles')->orderBy('created_at', 'desc')->take(5)->get();
        \Cache::forever('hot_articles', $hot_articles);
    }
    return $hot_articles;
}

/**
 * 获取配置的头条号
 * @param  [type] $uid [description]
 * @return [type]      [description]
 */
function jzGetToutiaos($uid)
{
    if (Session::has('toutiaos')) {
        return Session::get('toutiaos');
    } else {
        $toutiaos = App\Toutiao::where('uid', $uid)->get();
        if (count($toutiaos)>0) {
            Session::put('toutiaos', $toutiaos);
            return $toutiaos;
        } else {
            return false;
        }
    }
}
/**
 * 获取特定头条号
 * @param  [type] $toutiao_id [description]
 * @return [type]             [description]
 */
function jzGetToutiao($toutiao_id)
{
    $toutiaos = jzGetToutiaos(\Auth::id());
    foreach ($toutiaos as $k =>$toutiao) {
        if ($toutiao->id == $toutiao_id) {
            return $toutiao;
        }
    }
    return false;
}

/**
 * 获取头条配置参数
 * @param  [type] $toutiao_id [description]
 * @param  [type] $type       [description]
 * @return [type]             [description]
 */
function jzGetToutiaoOption($toutiao_id, $type=null)
{
    $toutiao = jzGetToutiao($toutiao_id);
    if ($toutiao) {
        if ($type==null) {
            return false;
        }
        return $toutiao[$type];
    } else {
        return false;
    }
}
/**
 * 头条授权url
 * @param  [type] $client_key [description]
 * @param  [type] $toutiao_id [description]
 * @return [type]             [description]
 */
function jzGetToutiaoAuthorizeUri($client_key, $toutiao_id)
{
    $uri = "https://open.snssdk.com/auth/authorize/?response_type=code&auth_only=1&client_key=".$client_key."&redirect_uri=https://media.jankz.com/toutiao_callback?status=".$toutiao_id;
    return $uri;
}
/**
 * 头条回调uri
 * @param  [type] $code       [description]
 * @param  [type] $toutiao_id [description]
 * @return [type]             [description]
 */
function jzGetToutiaoCallbackUri($code, $toutiao_id)
{
    $client_key = jzGetToutiaoOption($toutiao_id, 'client_key');
    $client_secret = jzGetToutiaoOption($toutiao_id, 'client_secret');
    $uri = "https://open.snssdk.com/auth/token/?code=".$code."&client_key=".$client_key."&client_secret=".$client_secret."&grant_type=authorize_code";
    return $uri;
}
/**
 * 获取access_token
 * @param  [type] $toutiao_id [description]
 * @return [type]             [description]
 */
function jzGetToutiaoAccessToken($toutiao_id)
{
    $toutiao = jzGetToutiao($toutiao_id);
    if ($toutiao->auth_info==null) {
        return false;
    }
    $authinfo = json_decode($toutiao->auth_info);
    $expire_in = $authinfo->expires_in;
    if ($expire_in >time()) {
        return $authinfo->access_token;
    }
    return false;
}

function jzShowSafely($str)
{
    $stop = strlen($str)-6;
    $replace = substr($str, 3, $stop);
    return str_replace($replace, '********', $str);
}

function jzToutiaoCategorys($type='array')
{
    $data = [
        'news_society'              =>        '社会',
        'news_entertainment'        =>        '娱乐',
        'movie'                     =>        '电影',
        'news_tech'                 =>        '科技',
        'digital'                   =>        '数码',
        'news_car'                  =>        '汽车',
        'news_sports'               =>        '体育',
        'news_finance'              =>        '财经',
        'news_military'             =>        '军事',
        'news_world'                =>        '国际',
        'news_fashion'              =>        '时尚',
        'marvel'                    =>        '奇葩',
        'news_game'                 =>        '游戏',
        'news_travel'               =>        '旅游',
        'news_baby'                 =>        '育儿',
        'fitness'                   =>        '瘦身',
        'news_regimen'              =>        '养生',
        'news_food'                 =>        '美食',
        'news_history'              =>        '历史',
        'news_discovery'            =>        '探索',
        'news_story'                =>        '故事',
        'news_essay'                =>        '美文',
        'emotion'                   =>        '情感',
        'news_health'               =>        '健康',
        'news_edu'                  =>        '教育',
        'news_home'                 =>        '家居',
        'news_house'                =>        '房产',
        'funny'                     =>        '搞笑',
        'news_astrology'            =>        '星座',
        'news_culture'              =>        '文化',
        'news_pet'                  =>        '宠物',
        'news_law'                  =>        '法制',
        'news_career'               =>        '职场',
        'comic'                     =>        '漫画',
        'news_comic'                =>        '动漫',
        'science_all'               =>        '科学',
        'news_design'               =>        '设计',
        'news_photography'          =>        '摄影',
        'news_collect'              =>        '收藏',
        'news_agriculture'          =>        '三农',
        'news_psychology'           =>        '心理'
    ];
    if ($type=='key') {
        return array_keys($data);
    }
    if ($type=='value') {
        return array_values($data);
    }
    if ($type=='array') {
        return $data;
    }
    if (array_key_exists($type, $data)) {
        return $data[$type];
    }
}
