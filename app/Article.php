<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    //评论 一对多
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
