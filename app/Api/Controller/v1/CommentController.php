<?php

namespace App\Api\Controller\v1;

use Illuminate\Http\Request;
use App\Comment;
use TimeAgo;

class CommentController extends CommonController
{
    public function __construct()
    {
    }
    public function getArticleComments(Request $request)
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $article_id = $request->input('article_id');
        $comments = Comment::where('article_id', $article_id)
            ->whereNull('reply_to')
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        foreach ($comments as $comment) {
            $comment->avatar = $comment->user->avatar;
            $comment->author = $comment->user->name;
            $comment->created_at = $timeAgo->inWords($comment->created_at);
        }
        dd($comments);
        return response()->json($comments);
    }
    public function getCommentReplys(Request $request)
    {
        $comment_id = $request->input('comment_id');
        $comments = Comment::where('reply_to', $comment_id)
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        return response()->json($comments);
    }
    public function postComment(Request $request)
    {
        $comment = new Comment;
        $comment->user_id = $request->input('user_id');
        $comment->article_id = $request->input('article_id');
        $comment->content = $request->input('content');
        $comment->type = $request->input('type');
        $comment->reply_to = $request->input('reply_to');
        $comment->save();
        return response()->json($comment);
    }
}
