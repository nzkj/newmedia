<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Comment;
use TimeAgo;

class CommentController extends CommonController
{
    public function __construct()
    {
    }
    public function getArticleComments(Request $request)
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $article_id = $request->input('article_id');
        $comments = Comment::where('article_id', $article_id)
            ->whereNull('reply_to')
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        foreach ($comments as $comment) {
            $comment->avatar = $comment->user->avatar;
            $comment->author = $comment->user->name;
            $comment->friendly_time = $timeAgo->inWords($comment->created_at);
        }

        return response()->json($comments);
    }
    public function getMoreComments(Request $request)
    {
        $article_id = $request->input('article_id');
        $last_id = $request->input('last_id');
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $article_id = $request->input('article_id');
        $comments = Comment::where('article_id', $article_id)
            ->whereNull('reply_to')
            ->where('id', '<', $last_id)
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        foreach ($comments as $comment) {
            $comment->avatar = $comment->user->avatar;
            $comment->author = $comment->user->name;
            $comment->friendly_time = $timeAgo->inWords($comment->created_at);
        }

        return response()->json($comments);
    }
    public function getCommentReplys(Request $request)
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $comment_id = $request->input('comment_id');
        $comments = Comment::where('reply_to', $comment_id)
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        foreach ($comments as $comment) {
            $comment->avatar = $comment->user->avatar;
            $comment->author = $comment->user->name;
            $comment->friendly_time = $timeAgo->inWords($comment->created_at);
        }
        return response()->json($comments);
    }
    public function postComment(Request $request)
    {
        $info['user_id'] = $request->input('user_id');
        $info['article_id'] = $request->input('article_id');
        $info['content'] = $request->input('content');
        $info['type'] = $request->input('type');
        $info['reply_to'] = $request->input('reply_to');
        $info['reply_to_user_id'] = $request->input('reply_to_user_id');
        $info['created_at'] = \Carbon\Carbon::now();
        $info['updated_at'] =  \Carbon\Carbon::now();
        $id = \DB::table('comments')->insertGetId($info);
        if ($id) {
            $comment = Comment::findOrFail($id);
            $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
            $comment->friendly_time = $timeAgo->inWords(date('Y-m-d H:i:s', time()));
            $comment->avatar = $comment->user->avatar;
            $comment->author = $comment->user->name;
            return response()->json($comment);
        }
    }
}
