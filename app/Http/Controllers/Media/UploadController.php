<?php

namespace App\Http\Controllers\Media;

use Dingo\Api\Auth\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UploadController extends CommonController
{
    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @return 上窜图片的url
     */
    public function picture(Request $request)
    {
        if ($request->hasFile('wangEditorH5File')) {
            if ($request->file('wangEditorH5File')->isValid()) {
                $file = $request->file('wangEditorH5File');
                $filename = $file->getClientOriginalName();
                $destinationPath = public_path('images/articles/'.date('Y-m-d').'/');
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $filename);

                $filename_back = '/images/articles/'.date('Y-m-d').'/'.$filename;
                if ($request->session()->has('article_pictures')) {
                    $request->session()->push('article_pictures', $filename_back);
                } else {
                    $article_pictures = [$filename_back];
                    $request->session()->put('article_pictures', $article_pictures);
                }
                return $filename_back;
            }
        } elseif ($request->hasFile('wangEditorPasteFile')) {
            if ($request->file('wangEditorPasteFile')->isValid()) {
                $file = $request->file('wangEditorPasteFile');
                $filename = $file->getClientOriginalName();
                $destinationPath = public_path('images/articles/'.date('Y-m-d').'/');
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $filename);

                $filename_back = '/images/articles/'.date('Y-m-d').'/'.$filename;
                if ($request->session()->has('article_pictures')) {
                    $request->session()->push('article_pictures', $filename_back);
                } else {
                    $article_pictures = [$filename_back];
                    $request->session()->put('article_pictures', $article_pictures);
                }
                return $filename_back;
            }
        } else {
            return 'error';
        }
    }
    public function pictures(Request $request)
    {
    }
    public function video(Request $request)
    {
    }
    /**
     * [avatar description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function avatar(Request $request)
    {
        $file = $request->file('avatar');
        $filename = 'avatar_'.time().substr($file->getClientOriginalName(), -5, 5);
        $destinationPath = public_path('images/avatar/'.date('Y-m-d').'/');
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        $file->move($destinationPath, $filename);
        //修改后名称
        $avatar_url = 'images/avatar/'.date('Y-m-d').'/mb_'.$filename;
        $savepath = public_path($avatar_url);
        $path = $destinationPath.$filename;
        Image::make($path)->fit(400, 400)->save($savepath);
        \File::delete($destinationPath.$filename);
        $info['avatar'] = '/'.$avatar_url;
        $info['updated_at'] = \Carbon\Carbon::now();
        $uid = \Auth::id();
        $res = \DB::table('users')->where('id', $uid)->update($info);
        \Auth::loginUsingId($uid);
        if ($res) {
            $result['status']=1;
            $result['info']='上传成功';
            $result['url']='/'.$avatar_url;
            return json_encode($result);
        } else {
            return json_encode(['status'=>0,'info'=>'失败']);
        }
        return false;
    }
}
