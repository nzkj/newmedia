<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Toutiao;

class ToutiaoController extends CommonController
{
    public function __construct()
    {
    }

    /**
     * 今日头条授权 获取code
     * @param  [type] $toutiao_id [description]
     * @return [type]             [description]
     */
    public function getAuthorize(Request $request)
    {
        $toutiao_id = $request->toutiao_id;
        $client_key = jzGetToutiaoOption($toutiao_id, 'client_key');
        $url = jzGetToutiaoAuthorizeUri($client_key, $toutiao_id);
        return json_encode(['status'=>1,'info'=>'success','url'=>$url]);
    }

    /**
     * 轻说回调 存储access_token
     * @return function [description]
     */
    public function callback()
    {
        $code = \Input::get('code');
        $toutiao_id = \Input::get('status');
        $expires_in = jzGetToutiaoOption($toutiao_id, 'expires_in');
        //检测token是否过期
        if ($expires_in>time()) {
            return view('media.account.api');
        }
        if ($code) {
            $uri = jzGetToutiaoCallbackUri($code, $toutiao_id);
            $client = new Client();
            $res = $client->request('GET', $uri);
            $ret = json_decode($res->getBody())->ret;
            if ($ret == 0) {
                $data = json_decode($res->getBody())->data;
                $access_token = $data->access_token;
                //保存Session
                \Session::put('toutiao_access_token', $access_token);
                $info['auth_info'] = json_encode($data);
                $info['screen_name'] = $data->screen_name;
                $info['open_id'] = $data->open_id;
                $info['access_token'] = $data->access_token;
                $info['expires_in'] = $data->expires_in;
                $info['uid_type'] = $data->uid_type;
                $info['toutiao_uid'] = $data->uid;
                $info['updated_at'] = \Carbon\Carbon::now();
                \DB::table('toutiaos')->where('id', $toutiao_id)->update($info);
                return view('media.toutiao.success');
            } else {
                return json_encode(['status'=>0,'info'=>'回去access_token失败！']);
            }
            /*
            *{#319 ▼
            *  +"data": {#321 ▼
            *    +"screen_name": "南哲科技微讯"
            *    +"open_id": 580558430015
            *    +"access_token": "0ce30ea3721c41ffa800348efd7046830015"
            *    +"expires_in": 1491265496
            *    +"uid_type": 12
            *    +"uid": 5441965216
            * }
            *  +"ret": 0
            *}
            */
        } else {
            return json_encode(['status'=>0,'info'=>'error code not find']);
        }
    }
    public function createApi(Request $request)
    {
        $toutiao = new Toutiao;
        $toutiao->display_name = $request->display_name;
        $toutiao->client_key = $request->client_key;
        $toutiao->client_secret = $request->client_secret;
        $toutiao->uid = \Auth::id();
        $toutiao->save();
        $url = url('account_api');
        return json_encode(['status'=>1,'info'=>'success','url'=>$url]);
    }
}
