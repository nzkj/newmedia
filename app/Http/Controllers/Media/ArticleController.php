<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;

use TimeAgo;

class ArticleController extends CommonController
{
    public function __construct()
    {
    }

    public function index($id)
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $article = Article::findOrFail($id);
        $hot_articles = jzHotArticles();
        $comments = Comment::where('article_id', $id)->whereNull('reply_to')->get();
        return view('media.articles.index', compact('article', 'hot_articles', 'comments', 'timeAgo'));
    }
    public function articleLists(Request $request)
    {
        $articles = Article::all();
        return view('media.articles.list', compact('articles'));
    }
    public function articleDelete($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();
        return json_encode(['status'=>1,'info'=>'删除成功','url'=>url('/article_lists')]);
    }
    public function articleEdit(Request $request, $id)
    {
        $method = $request->method();
        if ($method=='POST') {
            dd($request->all());
        } elseif ($method=='GET') {
            $article = Article::findOrFail($id);
            if ($article) {
                return view('media.articles.edit', compact('article'));
            } else {
                return 'not find or delete';
            }
        } else {
            return 'method is not author';
        }
    }
    public function articlePush($id)
    {
        $article = Article::findOrFail($id);
        return view('media.articles.push', compact('article'));
    }
}
