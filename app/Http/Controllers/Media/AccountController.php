<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\Request;
use App\Toutiao;
use TimeAgo;

class AccountController extends CommonController
{
    public function __construct()
    {
    }

    public function index()
    {
        return view('media.account.index');
    }

    public function avatar()
    {
        return view('media.account.avatar');
    }

    public function info()
    {
        return view('media.account.info');
    }
    public function api()
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $uid = \Auth::id();
        $toutiaos = Toutiao::where('uid', $uid)->get();
        return view('media.account.api', compact('toutiaos', 'timeAgo'));
    }
}
