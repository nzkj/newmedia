<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use TimeAgo;

class CommentController extends CommonController
{
    public function __construct()
    {
    }
    /**
     * 评论管理
     * @return [type] [description]
     */
    public function commentManager()
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');

        $comments = Comment::all();
        return view('media.comments.index', compact('comments', 'timeAgo'));
    }
    /**
     * 发表评论
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function commentCreate(Request $request)
    {
        $comment = new Comment;
        $comment->uid = Auth::id();
        $comment->author = Auth::user()->name;
        $comment->avatar = Auth::user()->avatar;
        $comment->content = $request->input('content');
        $comment->article_id = $request->article_id;
        $comment->reply_to = $request->reply_to;
        $comment->status = 1;
        $comment->save();
        return json_encode(['status'=>'1','info'=>'success']);
    }
    /**
     * 获取回复  ajax load
     * @param  [type] $comment_uid [description]
     * @param  [type] $author      [description]
     * @return [type]              [description]
     */
    public function getReply($comment_uid, $author)
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $replys = Comment::where('reply_to', $comment_uid)->get();
        return view('media.load.get_reply', compact('replys', 'timeAgo', 'author'));
    }
    /**
     * 获取评论 ajax load
     * @param  [type]  $article_id 文章ID
     * @param  integer $last_id       偏移量
     * @param  integer $limit      抽取限制
     * @return [type]              评论
     */
    public function getComments($article_id, $last_id = 0, $limit=10)
    {
        $comments = Comment::where('article_id', $article_id)
            ->where('status', 1)
            ->where('id', '>', $last_id)
            ->take($limit)
            ->orderBy('created_at', decs)
            ->get();
        return view('media.load.get_comments');
    }
}
