<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\Request;
use App\Article;
use App\Carousel;
use TimeAgo;

class IndexController extends CommonController
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        // \Session::flush();

        $slides = Carousel::where('status', 0)->take(10)->get();
        $articles = Article::all();
        $hot_articles =  jzHotArticles();
        return view('media.index.index', compact('articles', 'hot_articles', 'slides'));
    }
    public function feedback()
    {
        return view('media.public.feedback');
    }
    public function more()
    {
        return view('media.public.more');
    }
    /**
     * 建议搜索
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function search(Request $request)
    {
        $query = $request->input('query');
        $articles = \DB::table('articles')->where('title', 'like', '%'.$query.'%')->get();
        $hot_articles =  jzHotArticles();
        return view('media.index.result', compact('articles', 'hot_articles'));
    }
    /**
     * 按内容类型阅读
     * @param  [type] $category [description]
     * @return [type]           [description]
     */
    public function articleByTag($tag)
    {
        $hot_articles =  jzHotArticles();

        $articles = Article::where('content_type', $tag)->paginate(10);
        return view('media.index.result', compact('articles', 'hot_articles'));
    }
    public function cooperate()
    {
        return view('media.public.cooperate');
    }
}
