<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\Request;
use App\Article;

class PublishController extends CommonController
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        return view('media.publish.index');
    }
    //发布文章
    public function article(Request $request)
    {
        if (\Session::has('article_pictures')) {
            $article_pictures = \Session::get('article_pictures');
        } else {
            \Session::put('article_pictures', []);
        }
        return view('media.publish.index');
    }
    //写入数据库
    public function articleCreate(Request $request)
    {
        $article = new Article;
        $article->title = $request->title;
        $article->content = htmlspecialchars($request->input('content'));
        $article->cover_type = $request->cover_type;
        $article->content_type = $request->content_type;
        $article->cover1 = $request->cover1;
        $article->cover2 = $request->cover2;
        $article->cover3 = $request->cover3;
        $article->is_publish = 1;
        $article->is_recommend = 0;
        $article->save();
        \Session::forget('article_pictures');
        return json_encode(['status'=>1,'info'=>'发布成功','url'=>url('article_lists')]);
    }
    //草稿
    public function articleDraft(Request $request)
    {
        $article = new Article;
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $article->cover_type = $request->input('cover_type');
        $article->content_type = $request->input('content_type');
        $article->save();
        return json_encode(['status'=>1,'info'=>'发布成功']);
    }

    /**
     * 文章封面
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function covers(Request $request)
    {
        $article_pictures = $request->session()->get('article_pictures');
        return view('media.load.article_pictures', compact('article_pictures'));
    }

    /**
     * 检测封面图片
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function coverCheck(Request $request)
    {
        if ($request->session()->get('article_pictures')==[]) {
            return json_encode(['status'=>0,'info'=>'请先在正文上传图片']);
        } else {
            return json_encode(['status'=>1,'info'=>'success']);
        }
    }
    public function video(Request $request)
    {
        return view('media.publish.publish_video');
    }
    public function pictures(Request $request)
    {
        return view('media.publish.publish_picture');
    }
}
