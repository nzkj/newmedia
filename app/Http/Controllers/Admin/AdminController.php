<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class AdminController extends CommonController
{
    public function __construct()
    {
    }
    public function index()
    {
        return view('admin.index.index');
    }
}
