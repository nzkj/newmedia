<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Article;
use TimeAgo;

class ArticleController extends CommonController
{
    public function __construct()
    {
    }
    public function list()
    {
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        $lists = Article::where('is_publish', 1)->paginate(10);
        return view('admin.articles.list', compact('lists', 'timeAgo'));
    }
}
