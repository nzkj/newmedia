<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class AuthController extends CommonController
{
    public function __construct()
    {
    }
    public function login(Request $request)
    {
        if ($request->method() == 'POST') {
            $email = $request->email;
            $password = $request->password;
            $user = \DB::table('admins')->where('email', $email)->first();
            if ($user) {
                if (\Hash::check($password, $user->password)) {
                    \Session::put('adminInfo', $user);
                    return json_encode(['status'=>1,'info'=>'登录成功！','url'=>url('/jzopenos/index')]);
                } else {
                    return json_encode(['status'=>0,'info'=>'密码错误！']);
                }
            } else {
                return json_encode(['status'=>0,'info'=>'管理员不存在！']);
            }
        } elseif ($request->method() == 'GET') {
            return view('admin.auth.login');
        } else {
            return false;
        }
    }
    public function logout()
    {
        \Session::forget('adminInfo');
        redirect('/');
    }
}
