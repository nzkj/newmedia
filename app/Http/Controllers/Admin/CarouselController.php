<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Carousel;
use TimeAgo;

class CarouselController extends CommonController
{
    public function __construct()
    {
    }
    public function list()
    {
        $lists = Carousel::paginate(10);
        $timeAgo = new TimeAgo('Asia/shanghai', 'zh-CN');
        return view('admin.carousel.list', compact('lists', 'timeAgo'));
    }
    public function add(Request $request)
    {
        $carousel = new Carousel;
        $carousel->uid = \Session('adminInfo')->id;
        $carousel->article_id =$request->id;
        $carousel->display_name = $request->title;
        $carousel->cover = $request->cover;
        $carousel->thumb_cover = $request->cover;
        $carousel->url = '/article/'.$request->id;
        $carousel->status = 0;
        $carousel->sort = 0;
        $res = $carousel->save();
        if ($res) {
            return json_encode(['status'=>1,'info'=>'加入成功']);
        }
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $carousel = Carousel::findOrFail($id);
        $res = $carousel->delete();
        if ($res) {
            $url = url('jzopenos/carousel/list');
            return json_encode(['status'=>1,'info'=>'删除成功','url'=>$url]);
        }
    }
    public function create()
    {
        if ($request->method()=='POST') {
            dd($request->all());
        } elseif ($request->method=='GET') {
            return view('admin.carousel.create');
        } else {
            return false;
        }
    }
    public function edit(Request $request)
    {
        if ($request->method()=='POST') {
            dd($request->all());
        } elseif ($request->method=='GET') {
            $id = $request->id;
            $carousel = Carousel::findOrFail($id);
            return view('admin.carousel.edit', compact('carousel'));
        } else {
            return false;
        }
    }
}
