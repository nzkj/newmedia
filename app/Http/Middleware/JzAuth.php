<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class JzAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('adminInfo')) {
            return redirect('/jzopenos/login');
        }
        return $next($request);
    }
}
