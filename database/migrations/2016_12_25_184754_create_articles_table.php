<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->comment('用户ID');
            $table->string('display_name', 100)->nullable()->comment('显示名称');
            $table->string('title')->comment('文章标题');
            $table->text('content')->comment('文章内容');
            $table->string('description')->nullable()->comment('文章摘要');
            $table->string('content_type', 16)->comment('文章类型');
            $table->string('tags', 100)->nullable()->comment('文章标签');
            $table->tinyInteger('cover_type')->comment('封面类型');
            $table->string('cover1')->comment('封面图片1');
            $table->string('cover2')->comment('封面图片2');
            $table->string('cover3')->comment('封面图片3');
            $table->tinyInteger('is_publish')->default(0)->comment('封面图片3');
            $table->tinyInteger('is_recommend')->default(0)->comment('是否推荐');
            $table->integer('readtimes')->default(0)->comment('阅读次数');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
