<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarouselsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carousels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->comment('管理员ID');
            $table->integer('article_id')->comment('文章ID');
            $table->string('display_name', 32)->nullable()->comment('显示名称');
            $table->string('cover')->comment('封面');
            $table->string('thumb_cover')->comment('缩略封面');
            $table->string('url')->comment('文章链接');
            $table->string('abstract')->nullable()->comment('摘要');
            $table->tinyInteger('status')->default(0)->comment('轮播态');
            $table->tinyInteger('sort')->default(0)->comment('排序值');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
